'''The MIT License (MIT)

Copyright (c) 2017 ActiveState Software Inc.

Written by Pete Garcin @rawktron

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.'''

from defs import *

import pygame
import utils
#import quiz
from quiz import *
from fireworks import *
from facedetection import OpenCV_Test
from utils import *
from actors import *
#from brain import Brain

import math
import leaderboard
import gameover
import time

pygame.mixer.init()
shootsfx = pygame.mixer.Sound('audio/HeroLaser.wav')
hitsfx = pygame.mixer.Sound('audio/EnemyHit.wav')
enemyshootsfx = pygame.mixer.Sound('audio/EnemyShoot.wav')
explodesfx = pygame.mixer.Sound('audio/ShipExplode.wav')
respawnsfx = pygame.mixer.Sound('audio/Respawn.wav')

# GameState object will return a new state object if it transitions
class GameState(object):
    def update(self, screen, event_queue, dt, clock, joystick, netmodel, vizmodel):
        return self

class Play(GameState):
    def __init__(self, trainingMode):
        if utils.trainedBrain:
            self.brain = utils.trainedBrain
        else:
            self.brain = Brain()
        self.enemyspeed = 16
        self.enemyBullets = pygame.sprite.Group()
        self.userBullets = pygame.sprite.Group()
        self.userGroup = pygame.sprite.Group()
        self.enemies = pygame.sprite.Group()
        self.player = Player(self.userBullets)
        self.enemy = Enemy(self.enemyBullets, self.brain,self.enemyspeed)
        self.userGroup.add(self.player)
        self.enemies.add(self.enemy)
        self.player.lives = MAX_LIVES
        self.score = 0
        self.spawntimer = 0
        self.spawnbreak = 8
        self.trainingMode = trainingMode

    def update(self, screen, event_queue, dt, clock, joystick, netmodel, vizmodel):
        self.player.update(screen, event_queue, dt,joystick)
        self.enemies.update(screen, event_queue, dt, (self.player.x,self.player.y), (self.player.velx,self.player.vely), self.trainingMode, netmodel)

        # Spawn new enemies
        self.spawntimer += dt
        if self.spawntimer > self.spawnbreak:
            self.spawnbreak = max(2,self.spawnbreak-0.5)
            self.enemyspeed = max(0,self.enemyspeed+2)
            self.enemies.add(Enemy(self.enemyBullets, self.brain,self.enemyspeed))
            self.spawntimer = 0

        if not(self.player.blinking):
            player_hit = pygame.sprite.spritecollide(self.player,self.enemyBullets, True)
            for bullet in player_hit:
                self.brain.record_hit(bullet)
                if not (self.trainingMode):
                    self.player.TakeDamage(20)
                self.player.playanim("hit",(bullet.rect.x,bullet.rect.y))

        if not(self.player.blinking and self.player.blinkon):
            self.userGroup.draw(screen)
        self.enemies.draw(screen)
        self.enemyBullets.update(dt)
        self.enemyBullets.draw(screen)
        self.userBullets.update(dt)
        self.userBullets.draw(screen)

        enemies_hit = pygame.sprite.groupcollide(self.enemies,self.userBullets,False,True)
        for enemy, bullets in enemies_hit.items():
            enemy.TakeDamage(10)
            for b in bullets:
                enemy.playanim("hit",(b.rect.x,b.rect.y))
            self.score += 50

        ## Update enemy animation frames
        for enemy in self.enemies:
            if enemy.anim:
                if enemy.anim.playing:
                    enemy.anim.update(screen,(enemy.x+enemy.animoffset[0],enemy.y+enemy.animoffset[1]),dt)
                else:
                    enemy.anim = None

        # Effects go here TODO make them a sprite layer
        if self.player.anim:
            if self.player.anim.playing:
                self.player.anim.update(screen,(self.player.x+self.player.animoffset[0],self.player.y+self.player.animoffset[1]),dt)
            else:
                self.player.anim = None

        #self.brain.draw(screen,vizmodel)

        #displaytext("Score: "+str(self.score), 16, SCORE_POSITION, 20, WHITE, screen)
        #displaytext("Health: "+str(self.player.health), 16, HEALTH_POSITION, 20, WHITE, screen)
        #displaytext("Lives: "+str(self.player.lives) , 16, LIVES_POSITION, 20, WHITE, screen)

        #displaytext("Neural Net Visualization", 16, 960, 20, WHITE, screen)

        for event in event_queue:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    if (self.trainingMode):
                        self.brain.learn()
                        utils.trainedBrain = self.brain
                        if (netmodel == 1):
                            self.brain.train()  # Train the tensorflow version
                    return QuizMenu(self.brain)

        if self.trainingMode:
            self.brain.learn()

        if not(self.player.alive()):
            if (self.trainingMode):
                self.brain.learn()
                utils.trainedBrain = self.brain
                return QuizMenu(None)
            else:
                return GameOver(self.score)

        # Win!
        if self.score > 999:
                return GameWin(self.score)

        return self

class PlayFireworks(GameState):
    def __init__(self, num_fireworks, thresh_time, jumpState,score, lives):
        self.score = score
        self.lives = lives
        self.jumpState = jumpState
        self.fireworks = Fireworks(num_fireworks, thresh_time)

    def update(self, screen, event_queue, dt, clock, joystick, netmodel, vizmodel):
        self.fireworks.show_works(screen)
        self.fireworks.add_time(dt)

        for event in event_queue:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_1:
                    pass
                elif event.key == pygame.K_2:
                    pass
                elif event.key == pygame.K_3:
                    pass
                elif event.key == pygame.K_4:
                    pass
                elif event.key == pygame.K_m:
                    return Menu(None)
                elif event.key == pygame.K_n:
                    return self.jumpState

        #if self.fireworks.done() == True:
        if self.fireworks.done == True:
            return self.jumpState

        return self

class PlayOpenCV_Test(GameState):
    def __init__(self, viz_num, score, lives):
        self.score = score
        self.lives = lives
        self.timer = 0
        self.viz = OpenCV_Test(viz_num)

    def update(self, screen, event_queue, dt, clock, joystick, netmodel, vizmodel):
        self.face_count = self.viz.start_viz(screen)
        self.score += self.face_count
        self.timer += dt
        if self.timer > 10:
            return QuizMenu(None)

        for event in event_queue:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_1:
                    pass
                elif event.key == pygame.K_2:
                    pass
                elif event.key == pygame.K_3:
                    pass
                elif event.key == pygame.K_4:
                    pass
                elif event.key == pygame.K_s:
                    pass
                    self.viz.flip_desmurf()
                elif event.key == pygame.K_m:
                    return Menu(None)
                elif event.key == pygame.K_q:
                    return None

        text_box(screen,"Score: {0:.0f}".format(self.score), SCORE_POSITION, 6, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, MENU_FONT_SIZE)

        if self.face_count > 10:
              print('face count 1000')
              return GameWin(self.score)

        return self

class PlayMathQuiz(GameState):
    def __init__(self, question_limit, difficulty, score, lives, previous_questions, next_stage):
        self.next_stage = next_stage
        self.previous_questions = previous_questions
        self.previous_questions_len = len(previous_questions)
        self.question_limit = question_limit
        self.score = score
        self.lives = lives
        self.this_answer = 'nil'
        self.quiz = MathQuiz(difficulty, previous_questions)
        self.difficulty = difficulty
        self.timer = 0
        self.bleed_timer = 0
        self.dying_timer = 0
        self.tick_timer = 0
        self.tick_timer_thresh = 100
        self.show_question = True
        self.show_question_timer = 3
        self.dying = False
        self.dead = False
        self.health = MAX_HEALTH
        self.wrong = 0

    def end(self):
          if self.next_stage == 1:
            return PlayQuizSpec(0,self.score,self.lives,1,'Visual')
          elif self.next_stage == 2:
            return PlayQuizSpec(0,self.score,self.lives,2,'Visual')
          elif self.next_stage == 3:
            return PlayQuizSpec(0,self.score,self.lives,3,'Visual')
          elif self.next_stage == 4:
            return PlayQuizSpec(0,self.score,self.lives,4,'Visual')
          elif self.next_stage == 'states':
            #return PlayQuizState(0,self.score,self.lives,['init'])
            return PlayQuizState(0,self.score,self.lives,['fiver'])
          else:
            return PlayOpenTDQuiz(self.score,self.lives)

    def update(self, screen, event_queue, dt, clock, joystick, netmodel, vizmodel):
        if self.lives < END_LIVES:
           return GameOver(self.score)
        elif self.dead == True:
            self.dead = False
            self.lives -= 1
            self.dying = False
            self.dying_timer = 0
            self.health = MAX_HEALTH
            self.wrong = 0
            self.tick_timer_thresh = 100
            self.timer = 0
            self.bleed_timer = 0
            self.show_question = True
            self.show_question_timer = 3
            if self.previous_questions_len >= self.question_limit:
          #    self.end()
              if self.next_stage == 1:
                return PlayQuizSpec(0,self.score,self.lives,1,'Visual')
              elif self.next_stage == 1.1:
                return PlayQuizSpec(0,self.score,self.lives,1,'Metric')
              elif self.next_stage == 2:
                return PlayQuizSpec(0,self.score,self.lives,2,'Visual')
              elif self.next_stage == 2.1:
                return PlayQuizSpec(0,self.score,self.lives,2,'Metric')
              elif self.next_stage == 3:
                return PlayQuizSpec(0,self.score,self.lives,3,'Visual')
              elif self.next_stage == 3.1:
                return PlayQuizSpec(0,self.score,self.lives,3,'Metric')
              elif self.next_stage == 4:
                return PlayQuizSpec(0,self.score,self.lives,4,'Visual')
              elif self.next_stage == 4.1:
                return PlayQuizSpec(0,self.score,self.lives,4,'Metric')
              elif self.next_stage == 'states':
                #return PlayQuizState(0,self.score,self.lives,['init'])
                return PlayQuizState(0,self.score,self.lives,['fiver'])
              else:
                return PlayOpenTDQuiz(self.score,self.lives)
            else:
              return PlayMathQuiz(self.question_limit, self.difficulty + 1, self.score, self.lives, self.quiz.get_previous_questions(), self.next_stage)
        elif self.dying == True:
            screen.fill(RED)
            text_box(screen, 'Lost a Life!', 'center', 'center', YELLOW,RED,YELLOW, MENU_FONT, HUGE_FONT_SIZE)
            #self.timer = 0
            self.dying_timer -= dt
            if self.dying_timer < 0:
                self.dead = True
            return self
        elif self.show_question == True:
            self.quiz.show_question(screen)
            self.show_question_timer -= dt
            if self.show_question_timer < 0:
                self.show_question = False
            return self
        else:
            # If not dead or dying tic the clock
            self.timer += dt
        this_lite_blu_num = int(abs(int(self.timer) - self.timer) * 255)
        this_lite_blu = (0, 0, this_lite_blu_num)
        if self.health < 500:
            self.tick_timer += (dt * 100)
            if self.tick_timer > self.tick_timer_thresh:
                self.tick_timer = 0
                shootsfx.play()
                if self.tick_timer_thresh > 10:
                    self.tick_timer_thresh -= 5
            if int(self.timer * 5) % 2 == 0:
                #screen.fill(LIGHT_BLUE)
                screen.fill(this_lite_blu)
        text_box(screen,"Score: {0:.0f}".format(self.score), SCORE_POSITION, 6, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, MENU_FONT_SIZE)
        text_box(screen,"Lives: {}".format(self.lives) , LIVES_POSITION, 6, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, MENU_FONT_SIZE)
        text_box(screen,"Health: {:.0f}".format(self.health), HEALTH_POSITION, 6, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, MENU_FONT_SIZE)

        self.quiz.start_quiz(screen)

        for event in event_queue:
            if event.type == pygame.KEYDOWN or event.type == pygame.JOYBUTTONDOWN:
                if (event.type == pygame.KEYDOWN and (event.key == pygame.K_1)) or (event.type == pygame.JOYBUTTONDOWN and (event.button == 1)):
                    self.this_answer = self.quiz.check_answer(1)
                elif (event.type == pygame.KEYDOWN and (event.key == pygame.K_2)) or (event.type == pygame.JOYBUTTONDOWN and (event.button == 2)):
                    self.this_answer = self.quiz.check_answer(2)
                elif (event.type == pygame.KEYDOWN and (event.key == pygame.K_3)) or (event.type == pygame.JOYBUTTONDOWN and (event.button == 3)):
                    self.this_answer = self.quiz.check_answer(3)
                elif (event.type == pygame.KEYDOWN and (event.key == pygame.K_4)) or (event.type == pygame.JOYBUTTONDOWN and (event.button == 4)):
                    self.this_answer = self.quiz.check_answer(4)
                elif event.key == pygame.K_m:
                    return Menu(None)


        if self.this_answer == 'correct':
            respawnsfx.play()
            self.this_answer = 'nil'
            self.score += self.health
            if self.previous_questions_len >= self.question_limit:
              if self.next_stage == 1:
                return PlayQuizSpec(0,self.score,self.lives,1,'Visual')
              elif self.next_stage == 1.1:
                return PlayQuizSpec(0,self.score,self.lives,1,'Metric')
              elif self.next_stage == 2:
                return PlayQuizSpec(0,self.score,self.lives,2,'Visual')
              elif self.next_stage == 2.1:
                return PlayQuizSpec(0,self.score,self.lives,2,'Metric')
              elif self.next_stage == 3:
                return PlayQuizSpec(0,self.score,self.lives,3,'Visual')
              elif self.next_stage == 3.1:
                return PlayQuizSpec(0,self.score,self.lives,3,'Metric')
              elif self.next_stage == 4:
                return PlayQuizSpec(0,self.score,self.lives,4,'Visual')
              elif self.next_stage == 4.1:
                return PlayQuizSpec(0,self.score,self.lives,4,'Metric')
              elif self.next_stage == 'states':
                #return PlayQuizState(0,self.score,self.lives,['init'])
                return PlayQuizState(0,self.score,self.lives,['fiver'])
              else:
                return PlayOpenTDQuiz(self.score,self.lives)
            else:
              return PlayMathQuiz(self.question_limit, self.difficulty + 1, self.score, self.lives, self.quiz.get_previous_questions(), self.next_stage)
        if self.this_answer == 'wrong':
            self.bleed_timer = 2
            hitsfx.play()
            self.this_answer = 'nil'
            self.wrong += WRONG_HIT
        if self.health < 0:
            self.dying_timer = 3
            self.dying = True
            explodesfx.play()
        self.health = (MAX_HEALTH - self.wrong - self.timer * 64)
        if self.lives < END_LIVES:
           return GameOver(self.score)

        if self.bleed_timer > 0:
            self.bleed_timer -= dt
            pygameSurface = pygame.image.load("images/bloodshot.png")
            pygameSurface_scaled = pygame.transform.scale(pygameSurface, (screen.get_width(),screen.get_height()))
            screen.blit(pygameSurface_scaled, (0,0))
        elif self.bleed_timer > 0:
            self.bleed_timer = 0

        return self

class PlayQuizState(GameState):
    def __init__(self, question, score, lives, states_left):
        self.score = score
        self.lives = lives
        self.this_answer = 'nil'
        self.quiz = StateQuiz(question, states_left)
        self.questioned = question
        self.timer = 0
        self.bleed_timer = 0
        self.dying_timer = 0
        self.tick_timer = 0
        self.tick_timer_thresh = 100
        self.show_question = True
        self.show_question_timer = 3
        self.dying = False
        self.dead = False
        self.health = MAX_HEALTH
        self.wrong = 0

    def update(self, screen, event_queue, dt, clock, joystick, netmodel, vizmodel):
        if self.lives < END_LIVES:
           return GameOver(self.score)
        elif self.dead == True:
            self.dead = False
            self.lives -= 1
            self.dying = False
            self.dying_timer = 0
            self.health = MAX_HEALTH
            self.wrong = 0
            self.tick_timer_thresh = 100
            self.timer = 0
            self.bleed_timer = 0
            self.show_question = True
            self.show_question_timer = 3
            states_left = self.quiz.get_states_left()
            if len(states_left) > 0:
                return PlayQuizState(self.questioned + 1, self.score, self.lives, states_left)
            else:
                return PlayMathQuiz(4, 5,self.score, self.lives,['init'],'opentdb')
            return self
        elif self.dying == True:
            screen.fill(RED)
            text_box(screen, 'Lost a Life!', 'center', 'center', YELLOW,RED,YELLOW, MENU_FONT, HUGE_FONT_SIZE)
            #self.timer = 0
            self.dying_timer -= dt
            if self.dying_timer < 0:
                self.dead = True
            return self
        elif self.show_question == True:
            self.quiz.show_question(screen)
            self.show_question_timer -= dt
            if self.show_question_timer < 0:
                self.show_question = False
            return self
        else:
            # If not dead or dying tic the clock
            self.timer += dt
        this_lite_blu_num = int(abs(int(self.timer) - self.timer) * 255)
        this_lite_blu = (0, 0, this_lite_blu_num)
        if self.health < 500:
            self.tick_timer += (dt * 100)
            if self.tick_timer > self.tick_timer_thresh:
                self.tick_timer = 0
                shootsfx.play()
                if self.tick_timer_thresh > 10:
                    self.tick_timer_thresh -= 5
            if int(self.timer * 5) % 2 == 0:
                #screen.fill(LIGHT_BLUE)
                screen.fill(this_lite_blu)

        text_box(screen,"Score: {0:.0f}".format(self.score), SCORE_POSITION, 6, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, MENU_FONT_SIZE)
        text_box(screen,"Lives: {}".format(self.lives) , LIVES_POSITION, 6, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, MENU_FONT_SIZE)
        text_box(screen,"Health: {:.0f}".format(self.health), HEALTH_POSITION, 6, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, MENU_FONT_SIZE)

        self.quiz.start_quiz(screen)

        for event in event_queue:
            if event.type == pygame.KEYDOWN or event.type == pygame.JOYBUTTONDOWN:
                if (event.type == pygame.KEYDOWN and (event.key == pygame.K_1)) or (event.type == pygame.JOYBUTTONDOWN and (event.button == 1)):
                    self.this_answer = self.quiz.check_answer(1)
                elif (event.type == pygame.KEYDOWN and (event.key == pygame.K_2)) or (event.type == pygame.JOYBUTTONDOWN and (event.button == 2)):
                    self.this_answer = self.quiz.check_answer(2)
                elif (event.type == pygame.KEYDOWN and (event.key == pygame.K_3)) or (event.type == pygame.JOYBUTTONDOWN and (event.button == 3)):
                    self.this_answer = self.quiz.check_answer(3)
                elif (event.type == pygame.KEYDOWN and (event.key == pygame.K_4)) or (event.type == pygame.JOYBUTTONDOWN and (event.button == 4)):
                    self.this_answer = self.quiz.check_answer(4)
                elif event.key == pygame.K_q:
                    return None
                elif event.key == pygame.K_m:
                    return Menu(None)

        if self.this_answer == 'correct':
            respawnsfx.play()
            self.this_answer = 'nil'
            self.score += self.health
            states_left = self.quiz.get_states_left()
            if len(states_left) > 0:
                return PlayQuizState(self.questioned + 1, self.score, self.lives, states_left)
            else:
                return PlayMathQuiz(4, 5,self.score, self.lives,['init'],'opentdb')
        if self.this_answer == 'wrong':
            self.bleed_timer = 2
            hitsfx.play()
            self.this_answer = 'nil'
            self.wrong += WRONG_HIT
        if self.health < 0:
            self.dying_timer = 3
            self.dying = True
            explodesfx.play()
        self.health = (MAX_HEALTH - self.wrong - self.timer * 64)
        if self.lives < END_LIVES:
           return GameOver(self.score)

        if self.bleed_timer > 0:
            self.bleed_timer -= dt
            pygameSurface = pygame.image.load("images/bloodshot.png")
            pygameSurface_scaled = pygame.transform.scale(pygameSurface, (screen.get_width(),screen.get_height()))
            screen.blit(pygameSurface_scaled, (0,0))
        elif self.bleed_timer > 0:
            self.bleed_timer = 0
        # END PlayOpenTD update
        return self

class PlayOpenTDQuiz(GameState):
    def __init__(self, score, lives):
        self.score = score
        self.timer = 0
        self.lives = lives
        self.wrong = 0
        self.bleed_timer = 0
        self.dying_timer = 0
        self.tick_timer = 0
        self.tick_timer_thresh = 100
        self.dying = False
        self.dead = False
        self.health = MAX_HEALTH
        self.this_answer = 'nil'
        self.quiz = OpenTD_Quiz()
        self.show_question = True
        self.show_question_timer = 3

    def update(self, screen, event_queue, dt, clock, joystick, netmodel, vizmodel):
        if self.lives < END_LIVES:
           return GameOver(self.score)
        elif self.dead == True:
            self.dead = False
            self.lives -= 1
            self.dying = False
            self.dying_timer = 0
            self.health = MAX_HEALTH
            self.wrong = 0
            self.tick_timer_thresh = 100
            self.timer = 0
            self.bleed_timer = 0
            self.show_question = True
            self.show_question_timer = 3
            if self.quiz.get_next_question():
                return self
            else:
                return PlayMathQuiz(4, 2,self.score, self.lives,['init'],'states')
        elif self.dying == True:
            screen.fill(RED)
            text_box(screen, 'Lost a Life!', 'center', 'center', YELLOW,RED,YELLOW, MENU_FONT, HUGE_FONT_SIZE)
            #self.timer = 0
            self.dying_timer -= dt
            if self.dying_timer < 0:
                self.dead = True
            return self
        elif self.show_question == True:
            self.quiz.show_question(screen)
            self.show_question_timer -= dt
            if self.show_question_timer < 0:
                self.show_question = False
            return self
        else:
            # If not dead or dying tic the clock
            self.timer += dt

        if self.bleed_timer > 0:
            screen.fill(RED)
        #this_lite_blu = (0, 0, int(math.cos(self.timer) * 255))
        #this_lite_blu_num = abs(int(math.cos(self.timer) * 255))
        #this_lite_blu_num = self.timer + int(self.timer) * 255
        this_lite_blu_num = int(abs(int(self.timer) - self.timer) * 255)
        #print(this_lite_blu_num)
        this_lite_blu = (0, 0, this_lite_blu_num)
        #print(this_lite_blu)
        if self.health < 500:
            self.tick_timer += (dt * 100)
            if self.tick_timer > self.tick_timer_thresh:
                self.tick_timer = 0
                shootsfx.play()
                if self.tick_timer_thresh > 10:
                    self.tick_timer_thresh -= 5
            if int(self.timer * 5) % 2 == 0:
                #screen.fill(LIGHT_BLUE)
                screen.fill(this_lite_blu)
        text_box(screen,"Score: {0:.0f}".format(self.score), SCORE_POSITION, 6, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, MENU_FONT_SIZE)
        text_box(screen,"Lives: {}".format(self.lives) , LIVES_POSITION, 6, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, MENU_FONT_SIZE)
        text_box(screen,"Health: {:.0f}".format(self.health), HEALTH_POSITION, 6, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, MENU_FONT_SIZE)

        if self.bleed_timer > 0:
            self.bleed_timer -= dt
            pygameSurface = pygame.image.load("images/bloodshot.png")
            pygameSurface_scaled = pygame.transform.scale(pygameSurface, (screen.get_width(),screen.get_height()))
            screen.blit(pygameSurface_scaled, (0,0))
        elif self.bleed_timer < 0:
            self.bleed_timer = 0
            self.dying_timer = 3
            self.dying = True
            explodesfx.play()

        self.quiz.draw_quiz(screen)

        for event in event_queue:
            if event.type == pygame.KEYDOWN or event.type == pygame.JOYBUTTONDOWN:
                if (event.type == pygame.KEYDOWN and (event.key == pygame.K_1)) or (event.type == pygame.JOYBUTTONDOWN and (event.button == 1)):
                    self.this_answer = self.quiz.check_answer(1)
                elif (event.type == pygame.KEYDOWN and (event.key == pygame.K_2)) or (event.type == pygame.JOYBUTTONDOWN and (event.button == 2)):
                    self.this_answer = self.quiz.check_answer(2)
                elif (event.type == pygame.KEYDOWN and (event.key == pygame.K_3)) or (event.type == pygame.JOYBUTTONDOWN and (event.button == 3)):
                    self.this_answer = self.quiz.check_answer(3)
                elif (event.type == pygame.KEYDOWN and (event.key == pygame.K_4)) or (event.type == pygame.JOYBUTTONDOWN and (event.button == 4)):
                    self.this_answer = self.quiz.check_answer(4)
                elif event.key == pygame.K_q:
                    return None
                elif event.key == pygame.K_m:
                    return Menu(None)

        if self.this_answer == 'correct':
            #print(self.this_answer)
            self.dying_timer = 0
            self.bleed_timer = 0
            self.this_answer = 'nil'
            respawnsfx.play()
            self.score += self.health
            self.health += HEALTH_POTION
            if self.health > MAX_HEALTH:
                self.health = MAX_HEALTH
            self.wrong = 0
            self.tick_timer_thresh = 100
            self.timer = 0
            self.show_question = True
            self.show_question_timer = 3
            if self.quiz.get_next_question():
                return self
            else:
                return PlayMathQuiz(4, 2,self.score, self.lives,['init'],'states')
        elif self.this_answer == 'wrong':
            self.bleed_timer = 2
            #print(self.this_answer)
            hitsfx.play()
            self.this_answer = 'nil'
            self.wrong += WRONG_HIT
        if self.health < 0:
            self.dying_timer = 3
            self.dying = True
            explodesfx.play()
        #self.health = (MAX_HEALTH - self.wrong - self.timer *100 (2000 - 1000/(1 - (1 / self.timer)))
        #self.health = (MAX_HEALTH - self.wrong - (1000 - 500/(1 - (1 / self.timer))))
        self.health = (MAX_HEALTH - self.wrong - self.timer * 64)

        if self.bleed_timer > 0:
            self.bleed_timer -= dt
            pygameSurface = pygame.image.load("images/bloodshot.png")
            pygameSurface_scaled = pygame.transform.scale(pygameSurface, (screen.get_width(),screen.get_height()))
            screen.blit(pygameSurface_scaled, (0,0))
        elif self.bleed_timer > 0:
            self.bleed_timer = 0
        # END PlayOpenTD update
        return self

class PlayQuizSpec(GameState):
    def __init__(self, questionList, score, lives, quizboxnum, quiztype):
        self.score = score
        self.lives = lives
        self.this_answer = 'nil'
        #self.quiz = QuizBlaster(question)
        self.quiz = QuizBlaster(questionList)
        self.quizboxnum = quizboxnum
        self.quiztype = quiztype
        #self.questioned = question
        self.questioned = 0
        self.questionList = self.quiz.return_questions()
        self.timer = 0
        self.wrong = 0
        self.bleed_timer = 0
        self.dying_timer = 0
        self.tick_timer = 0
        self.tick_timer_thresh = 100
        self.dying = False
        self.dead = False
        self.health = MAX_HEALTH
        self.show_question = True
        self.show_question_timer = 3

    def update(self, screen, event_queue, dt, clock, joystick, netmodel, vizmodel):
        if self.lives < END_LIVES:
           return GameOver(self.score)
        elif self.dead == True:
            self.lives -= 1
            if self.lives < END_LIVES:
               return GameOver(self.score)
            else:
                self.quiz.delete_question(0, self.quizboxnum, self.quiztype)
                if self.quiz.get_num_rows_spec(self.quizboxnum, self.quiztype) > 0:
                    print(self.questionList[0])
                    self.questionList = self.quiz.return_questions()
                    return PlayQuizSpec(self.questionList, self.score, self.lives, self.quizboxnum, self.quiztype)
                else:
                    return PlayMathQuiz(4, 2,self.score, self.lives,['init'],'states')
        elif self.dying == True:
            screen.fill(RED)
            text_box(screen, 'Lost a Life!', 'center', 'center', YELLOW,RED,YELLOW, MENU_FONT, HUGE_FONT_SIZE)
            self.dying_timer -= dt
            if self.dying_timer < 0:
                self.dead = True
            return self
        elif self.show_question == True:
            self.quiz.show_question(screen, self.quizboxnum, self.quiztype)
            self.show_question_timer -= dt
            if self.show_question_timer < 0:
                self.show_question = False
            return self
        else:
            # If not dead or dying tic the clock
            self.timer += dt

        if self.bleed_timer > 0:
            screen.fill(RED)
        this_lite_blu_num = int(abs(int(self.timer) - self.timer) * 255)
        this_lite_blu = (0, 0, this_lite_blu_num)
        if self.health < 500:
            self.tick_timer += (dt * 100)
            if self.tick_timer > self.tick_timer_thresh:
                self.tick_timer = 0
                shootsfx.play()
                if self.tick_timer_thresh > 10:
                    self.tick_timer_thresh -= 5
            if int(self.timer * 5) % 2 == 0:
                screen.fill(this_lite_blu)
        text_box(screen,"Score: {0:.0f}".format(self.score), SCORE_POSITION, 6, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, MENU_FONT_SIZE)
        text_box(screen,"Lives: {}".format(self.lives) , LIVES_POSITION, 6, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, MENU_FONT_SIZE)
        text_box(screen,"Health: {:.0f}".format(self.health), HEALTH_POSITION, 6, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, MENU_FONT_SIZE)

        self.quiz.start_quiz_spec(screen, self.quizboxnum, self.quiztype)

        for event in event_queue:
            if event.type == pygame.KEYDOWN or event.type == pygame.JOYBUTTONDOWN:
                if (event.type == pygame.KEYDOWN and (event.key == pygame.K_1)) or (event.type == pygame.JOYBUTTONDOWN and (event.button == 1)):
                    self.this_answer = self.quiz.check_answer(1)
                elif (event.type == pygame.KEYDOWN and (event.key == pygame.K_2)) or (event.type == pygame.JOYBUTTONDOWN and (event.button == 2)):
                    self.this_answer = self.quiz.check_answer(2)
                elif (event.type == pygame.KEYDOWN and (event.key == pygame.K_3)) or (event.type == pygame.JOYBUTTONDOWN and (event.button == 3)):
                    self.this_answer = self.quiz.check_answer(3)
                elif (event.type == pygame.KEYDOWN and (event.key == pygame.K_4)) or (event.type == pygame.JOYBUTTONDOWN and (event.button == 4)):
                    self.this_answer = self.quiz.check_answer(4)
                elif event.key == pygame.K_m:
                    return Menu(None)


        if self.this_answer == 'correct':
            #print(self.this_answer)
            self.this_answer = 'nil'
            respawnsfx.play()
            self.score += self.health
            self.health += HEALTH_POTION
            if self.health > MAX_HEALTH:
                self.health = MAX_HEALTH
            self.wrong = 0
            self.tick_timer_thresh = 100
            self.timer = 0
            self.show_question = True
            self.show_question_timer = 3
            self.quiz.delete_question(0, self.quizboxnum, self.quiztype)
            if self.quiztype == 'Visual':
                if self.quiz.get_num_rows_spec(self.quizboxnum, self.quiztype) > 0:
                    #jumpState = PlayQuizSpec(self.questioned + 1, self.score, self.lives, self.quizboxnum, self.quiztype)
                    #return PlayFireworks(CORRECT_ANSWER_FIREWORKS, CORRECT_ANSWER_THRESH, jumpState, 0, MAX_LIVES)
                    #return PlayQuizSpec(self.questioned + 1, self.score, self.lives, self.quizboxnum, self.quiztype)
                    self.questionList = self.quiz.return_questions()
                    return PlayQuizSpec(self.questionList, self.score, self.lives, self.quizboxnum, self.quiztype)
                else:
                    jumpState = PlayMathQuiz(2, 3,self.score, self.lives,['init'], self.quizboxnum + 0.1 )
                    return PlayFireworks(GAMEWIN_FIREWORKS, GAMEWIN_THRESH, jumpState, 0, MAX_LIVES)
            elif self.quiztype == 'Metric':
                if self.quiz.get_num_rows_spec(self.quizboxnum, self.quiztype) > 0:
                    #return PlayQuizSpec(self.questioned + 1, self.score, self.lives, self.quizboxnum, self.quiztype)
                    print(self.questionList[0])
                    self.questionList = self.quiz.return_questions()
                    return PlayQuizSpec(self.questionList, self.score, self.lives, self.quizboxnum, self.quiztype)
                else:
                    jumpState = PlayMathQuiz(3, 4,self.score, self.lives,['init'],'states')
                    return PlayFireworks(GAMEWIN_FIREWORKS, GAMEWIN_THRESH, jumpState, 0, MAX_LIVES)
        if self.this_answer == 'wrong':
            self.bleed_timer = 2
            #print(self.this_answer)
            hitsfx.play()
            self.this_answer = 'nil'
            self.wrong += WRONG_HIT
        if self.health < 0:
            self.dying_timer = 3
            self.dying = True
            explodesfx.play()
        #self.health = (MAX_HEALTH - self.wrong - self.timer *100 (2000 - 1000/(1 - (1 / self.timer)))
        #self.health = (MAX_HEALTH - self.wrong - (1000 - 500/(1 - (1 / self.timer))))
        self.health = (MAX_HEALTH - self.wrong - self.timer * 64)

        if self.bleed_timer > 0:
            self.bleed_timer -= dt
            pygameSurface = pygame.image.load("images/bloodshot.png")
            pygameSurface_scaled = pygame.transform.scale(pygameSurface, (screen.get_width(),screen.get_height()))
            screen.blit(pygameSurface_scaled, (0,0))
        elif self.bleed_timer > 0:
            self.bleed_timer = 0
        return self

class PlayQuiz(GameState):
    def __init__(self, question, score, lives):
        self.score = score
        self.lives = lives
        self.this_answer = 'nil'
        self.quiz = QuizBlaster(question)
        self.questioned = question

    def update(self, screen, event_queue, dt, clock, joystick, netmodel, vizmodel):
        #displaytext("Score: "+str(self.score), 16, SCORE_POSITION, 20, YELLOW, screen)
        #displaytext("Lives: "+str(self.lives) , 16, LIVES_POSITION, 20, YELLOW, screen)

        self.quiz.start_quiz(screen)

        for event in event_queue:
            if event.type == pygame.KEYDOWN or event.type == pygame.JOYBUTTONDOWN:
                if (event.type == pygame.KEYDOWN and (event.key == pygame.K_1)) or (event.type == pygame.JOYBUTTONDOWN and (event.button == 1)):
                    self.this_answer = self.quiz.check_answer(1)
                elif (event.type == pygame.KEYDOWN and (event.key == pygame.K_2)) or (event.type == pygame.JOYBUTTONDOWN and (event.button == 2)):
                    self.this_answer = self.quiz.check_answer(2)
                elif (event.type == pygame.KEYDOWN and (event.key == pygame.K_3)) or (event.type == pygame.JOYBUTTONDOWN and (event.button == 3)):
                    self.this_answer = self.quiz.check_answer(3)
                elif (event.type == pygame.KEYDOWN and (event.key == pygame.K_4)) or (event.type == pygame.JOYBUTTONDOWN and (event.button == 4)):
                    self.this_answer = self.quiz.check_answer(4)
                elif event.key == pygame.K_m:
                    return Menu(None)

        if self.this_answer == 'correct':
            self.score += 1
            if self.questioned + 1 < self.quiz.num_rows:
                print('jump')
                jumpState = PlayQuiz(self.questioned + 1, self.score, self.lives)
                return PlayFireworks(CORRECT_ANSWER_FIREWORKS, CORRECT_ANSWER_THRESH, jumpState, 0, MAX_LIVES)
            else:
                jumpState = PlayOpenTDQuiz(self.score, self.lives)
                return PlayFireworks(GAMEWIN_FIREWORKS, GAMEWIN_THRESH, jumpState, 0, MAX_LIVES)
        elif self.this_answer == 'wrong':
            self.lives -= 1
            self.this_answer = 'nil'
            if self.lives < END_LIVES:
                return GameOver(self.score)

        if self.score > 999:
                return GameWin(self.score)

        return self

class GameOver(GameState):
    def __init__(self,score):
        self.score = int(score)
        self.count = 0
        self.timer = 0
        self.game_over = game_over_screen
        self.game_over_x = self.game_over.get_width() /2
        self.game_over_y = self.game_over.get_height() /2
    def update(self,screen,event_queue,dt,clock, joystick, netmodel, vizmodel):
        self.game_over_x_pos = screen.get_width()  / 2 - self.game_over_x
        self.game_over_y_pos = screen.get_height() / 2 - self.game_over_y
        #screen.fill(YELLOW)
        #text_box(screen, 'GAME OVER!!!.', 'center', 'center', YELLOW,RED,YELLOW, MENU_FONT, HUGE_FONT_SIZE)
        screen.blit(self.game_over,(self.game_over_x_pos, self.game_over_y_pos))
        pygame.event.clear()
        self.count += 1
        self.timer += dt
        if self.timer > 3:
            print('time over')
            nextState = QuizMenu(None)
            nextState = EnterName_GameOver(self.score)
        else:
            nextState = self
        return nextState

class EnterName_GameOver(GameState):
    def __init__(self,score):
        self.score = score
        self.name = ""
        gameover.pressed = ""
    def update(self,screen,event_queue,dt,clock, joystick, netmodel, vizmodel):
        nextState = self
        self.name = gameover.enter_text(event_queue,screen, 8)
        for event in event_queue:
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_RETURN:
                    self.name = gameover.pressed
                    try:
                        leaderboard.StoreScore(self.name,self.score)
                    except:
                        print('store error')
                    nextState = Leaderboard(self.name)
            elif (event.type == pygame.KEYDOWN and (event.key == pygame.K_4)) or (event.type == pygame.JOYBUTTONDOWN and (event.button == 4)):
                pass
            if gameover.submit_name == True:
                # button 4 pressed end
                self.name = gameover.pressed
                try:
                    leaderboard.StoreScore(self.name,self.score)
                except:
                    print('store error')
                nextState = Leaderboard(self.name)
        return nextState

class GameWin(GameState):
    def __init__(self,score):
        self.score = score
        self.name = ""
        gameover.pressed = ""
    def update(self,screen,event_queue,dt,clock, joystick, netmodel, vizmodel):
        nextState = self
        self.name = gameover.enter_text(event_queue,screen, 8)
        for event in event_queue:
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_RETURN:
                    self.name = gameover.pressed
                    try:
                        leaderboard.StoreScore(self.name,self.score)
                    except:
                        print('store error')
                    try:
                        leaderboard.StoreGrandTotal(self.score)
                    except:
                        print('grandtotal store error')
                    nextState = Leaderboard(self.name)
        return nextState

class RedBull_Splash(GameState):
    def __init__(self):
        self.count = 0
        self.timer = 0
        self.redbull_splash_x = redbull_splash.get_width() /2
        self.redbull_splash_y = redbull_splash.get_height() /2
    def update(self,screen,event_queue,dt,clock, joystick, netmodel, vizmodel):
        self.redbull_splash_x_pos = screen.get_width()  / 2 - self.redbull_splash_x
        self.redbull_splash_y_pos = screen.get_height() / 2 - self.redbull_splash_y
        screen.blit(redbull_splash,(self.redbull_splash_x_pos, self.redbull_splash_y_pos))
        pygame.event.clear()
        self.count += 1
        self.timer += dt
        if self.timer > 3:
            nextState = QuizMenu(None)
        else:
            nextState = self
        return nextState

class Leaderboard(GameState):
    def __init__(self,name):
        self.name = name
        self.timer = 0
        try:
            self.highscores = leaderboard.GetScores()
        except:
            print('scores error')
            self.highscores = [('data_unavailable',0)]
        try:
            self.grandtotal = leaderboard.GetGrandTotal()
        except:
            print('grand total error')
            self.grandtotal = 0

    def update(self,screen,event_queue,dt,clock,joystick, netmodel, vizmodel):
        self.timer += dt
        if self.timer > 8:
            nextState = RedBull_Splash()
        else:
            nextState = self
        try:
            leaderboard.DisplayLeaderBoard(screen,self.highscores,self.grandtotal,self.name)
        except:
            print('leader error')
            nextState = RedBull_Splash()
        for event in event_queue:
            if event.type == pygame.KEYDOWN:
                nextState = QuizMenu(None)


        return nextState

class QuizMenu(GameState):
    def __init__(self, brain):
        self.menu_selection = 2
        self.timer = 0
        self.brain = brain
        #print('QuizMenu')
        if X_BOUND == 1280:
          #print(1280)
          self.logo = main_logo
          self.logo_x = self.logo.get_width() /2
          self.logo_y = self.logo.get_height() /2
        if X_BOUND == 800:
          #print(800)
          #self.logo = pygame.image.load("images/redbull_challenge333.png")
          #self.logo = pygame.transform.scale(main_logo, (int(X_BOUND / 2), int(Y_BOUND / 2)))
          self.logo = main_logo
          self.logo_x = self.logo.get_width() /2
          self.logo_y = self.logo.get_height() /2
        else:
          print('unknown resolution')
          #self.logo = pygame.transform.scale(main_logo, (int(X_BOUND / 2), int(Y_BOUND / 2)))
          self.logo = main_logo
          self.logo_x = self.logo.get_width() /2
          self.logo_y = self.logo.get_height() /2

    def update(self, screen, event_queue, dt,clock,joystick, netmodel, vizmodel):
        self.timer += dt
        self.logo_x_pos = screen.get_width()  / 2 - self.logo_x
        self.logo_y_pos = screen.get_height() / 2 - self.logo_y
        if self.timer > 8:
            #print('returning leaderboard')
            return Leaderboard(' ')
            #return PlayOpenCV_Test(1,0,0)
            #pass
        #print(screen.get_width() / 2 - self.logo_x,screen.get_height() / 2  - self.logo_y)
        screen.blit(self.logo,(self.logo_x_pos, self.logo_y_pos))
        #screen.blit(self.logo, (185.0, 114.0))
        #screen.blit(self.logo, (0, 0))

        nextState = self
        #text_box(screen,"Play", screen.get_width() / 2 - 20, screen.get_height() * 7 / 8, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, MENU_FONT_SIZE)

        # Each game state processes its own input queue in its own way to avoid messy input logic
        for event in event_queue:
            if event.type == pygame.KEYDOWN or event.type == pygame.JOYBUTTONDOWN:
                if (event.type == pygame.KEYDOWN and (event.key == pygame.K_1)) or (event.type == pygame.JOYBUTTONDOWN and (event.button == 1)):
                    #nextState = PlayQuizSpec(0,0,MAX_LIVES,1,'Visual')
                    nextState = PlayMathQuiz(2,1,0,MAX_LIVES,['init'],1)
                elif (event.type == pygame.KEYDOWN and (event.key == pygame.K_2)) or (event.type == pygame.JOYBUTTONDOWN and (event.button == 2)):
                    #nextState = PlayQuizSpec(0,0,MAX_LIVES,2,'Visual')
                    nextState = PlayMathQuiz(2,1,0,MAX_LIVES,['init'],2)
                elif (event.type == pygame.KEYDOWN and (event.key == pygame.K_3)) or (event.type == pygame.JOYBUTTONDOWN and (event.button == 3)):
                    #nextState = PlayQuizSpec(0,0,MAX_LIVES,3,'Visual')
                    nextState = PlayMathQuiz(2,1,0,MAX_LIVES,['init'],3)
                elif (event.type == pygame.KEYDOWN and (event.key == pygame.K_4)) or (event.type == pygame.JOYBUTTONDOWN and (event.button == 4)):
                    #nextState = PlayQuizSpec(0,0,MAX_LIVES,4,'Visual')
                    nextState = PlayMathQuiz(2,1,0,MAX_LIVES,['init'],4)
                elif (event.type == pygame.KEYDOWN and (event.key == pygame.K_5)) or (event.type == pygame.JOYBUTTONDOWN and (event.button == 5)):
                    nextState = PlayQuiz(0,0,MAX_LIVES)
                elif (event.type == pygame.KEYDOWN and (event.key == pygame.K_n)):
                    nextState = Menu(None)
                elif (event.type == pygame.KEYDOWN and (event.key == pygame.K_s)):
                    #nextState = PlayQuizState(0,0,MAX_LIVES,['init'])
                    nextState = PlayQuizState(0,0,MAX_LIVES,['fiver'])
                elif (event.type == pygame.KEYDOWN and (event.key == pygame.K_v)):
                    nextState = PlayOpenCV_Test(0, 0, MAX_LIVES)
                elif (event.type == pygame.KEYDOWN and (event.key == pygame.K_m)):
                    nextState = PlayMathQuiz(2,1,0,MAX_LIVES,['init'],1)
                elif (event.type == pygame.KEYDOWN and (event.key == pygame.K_f)):
                    #jumpState = Menu(None)
                    jumpState = PlayMathQuiz(2,1,0,MAX_LIVES,['init'],2)
                    nextState = PlayFireworks(GAMEWIN_FIREWORKS, GAMEWIN_THRESH, jumpState, 0, MAX_LIVES)
                elif (event.type == pygame.KEYDOWN and (event.key == pygame.K_o)):
                    #jumpState = PlayOpenTDQuiz(0,MAX_LIVES, 1)
                    #nextState = PlayFireworks(GAMEWIN_FIREWORKS, GAMEWIN_THRESH, jumpState, 0, MAX_LIVES)
                    nextState = PlayOpenTDQuiz(0,MAX_LIVES)
                elif (event.type == pygame.KEYDOWN and (event.key == pygame.K_q)):
                    nextState = None
        return nextState

class QuizMenu1(GameState):
    def __init__(self, brain):
        self.menu_selection = 2
        self.brain = brain
        #self.logo = pygame.transform.scale(main_logo, (int(X_BOUND / 2), int(Y_BOUND / 2)))
        self.logo = main_logo
        print('QuizMenu1')

    def update(self, screen, event_queue, dt,clock,joystick, netmodel, vizmodel):
        screen.blit(self.logo,(screen.get_width() / 2 - 265,screen.get_height() * 3 / 4-500))

        nextState = self
        #displaytext('Play', 72, screen.get_width() / 2 - 20, screen.get_height() * 3 / 4
        #            - 80, YELLOW, screen)

        # Each game state processes its own input queue in its own way to avoid messy input logic
        for event in event_queue:
            if event.type == pygame.KEYDOWN or event.type == pygame.JOYBUTTONDOWN:
                nextState = PlayQuizSpec(0,0,MAX_LIVES,1,'Visual')
        return nextState

class QuizMenuSpec(GameState):
    def __init__(self, brain, spec):
        self.menu_selection = 2
        self.brain = brain
        self.spec = spec
        #self.logo = pygame.transform.scale(main_logo, (int(X_BOUND / 2), int(Y_BOUND / 2)))
        print('QuizMenuSpec')
        self.logo = main_logo

    def update(self, screen, event_queue, dt,clock,joystick, netmodel, vizmodel):
        screen.blit(self.logo,(screen.get_width() / 2 - 265,screen.get_height() * 3 / 4-500))

        nextState = self
        #displaytext('Play', 72, screen.get_width() / 2 - 20, screen.get_height() * 3 / 4
        #            - 80, YELLOW, screen)

        # Each game state processes its own input queue in its own way to avoid messy input logic
        for event in event_queue:
            if event.type == pygame.KEYDOWN or event.type == pygame.JOYBUTTONDOWN:
                nextState = PlayQuizSpec(0,0,MAX_LIVES,self.spec,'Visual')
        return nextState

# Draws the menu on screen.
# This is a class that is just instantiated
# While that object exists, it processes stuff
# Only one "GameState" object can exist at one time
class Menu(GameState):
    def __init__(self, brain):
        self.menu_selection = 2
        self.brain = brain
        self.logo = pygame.image.load("art/neuro-blast_logo.png")
        self.intel = pygame.image.load("art/Intel-logo_blue.png")
        self.activestate = pygame.image.load("art/as-logo.png")
        self.intel = pygame.transform.smoothscale(self.intel,(int(self.intel.get_width()/2),int(self.intel.get_height()/2)))
        self.activestate = pygame.transform.smoothscale(self.activestate,(int(self.activestate.get_width()/2),int(self.activestate.get_height()/2)))

    def update(self, screen, event_queue, dt,clock,joystick, netmodel, vizmodel):
        # Logos/titles
        screen.blit(self.logo,(screen.get_width() / 2 - 265,screen.get_height() * 3 / 4-500))
        screen.blit(self.intel,(screen.get_width() / 4 - 300,screen.get_height()-130))
        screen.blit(self.activestate,(screen.get_width() - 980,screen.get_height() - 130))

        nextState = self
        #displaytext('Play', MENU_FONT_SIZE, screen.get_width() / 2 - 20, screen.get_height() * 3 / 4
        #            - 80, WHITE, screen)
        #displaytext('Train', MENU_FONT_SIZE, screen.get_width() / 2 - 20, screen.get_height() * 3 / 4
        #            - 40, WHITE, screen)
        #displaytext('Exit', MENU_FONT_SIZE, screen.get_width() / 2 - 20, screen.get_height() * 3 / 4,
        #            WHITE, screen)
        #jdisplaytext(u'\u00bb', MENU_FONT_SIZE, screen.get_width() / 2 - 60, screen.get_height() * 3 / 4
        #            - 40*self.menu_selection, WHITE, screen)

        # Each game state processes its own input queue in its own way to avoid messy input logic
        for event in event_queue:
            if event.type == pygame.KEYDOWN or event.type == pygame.JOYBUTTONDOWN:
                if (event.type == pygame.KEYDOWN and (event.key == pygame.K_DOWN)) or (event.type == pygame.JOYBUTTONDOWN and (event.button == 1)) or (event.type == pygame.JOYAXISMOTION and (event.axis == 1 or event.value >= DEADZONE)):
                    self.menu_selection -= 1
                    if self.menu_selection == -1:
                        self.menu_selection = 2
                if (event.type == pygame.KEYDOWN and (event.key == pygame.K_UP)) or (event.type == pygame.JOYBUTTONDOWN and (event.button == 0)) or (event.type == pygame.JOYAXISMOTION and (event.axis == 1 or event.value <= -DEADZONE)):
                    self.menu_selection += 1
                    if self.menu_selection == 3:
                        self.menu_selection = 0
                if (event.type == pygame.KEYDOWN and event.key == pygame.K_RETURN) or (event.type == pygame.JOYBUTTONDOWN and event.button == 11):
                    if self.menu_selection == 2:
                        nextState = Play(False)
                    elif self.menu_selection == 1:
                        nextState = QuizMenu(True)
                    else:
                        nextState = None
                if (event.type == pygame.KEYDOWN and event.key == pygame.K_x):
                    self.ExportModel()
                if (event.type == pygame.KEYDOWN and event.key == pygame.K_d):
                    self.DumpData()
                if (event.type == pygame.KEYDOWN and event.key == pygame.K_w):
                    self.DumpWeights()
        return nextState

    def ExportModel(self):
        import keras.backend as K
        from tensorflow.python.saved_model import builder as saved_model_builder
        from tensorflow.python.saved_model import utils
        from tensorflow.python.saved_model import tag_constants, signature_constants
        from tensorflow.python.saved_model.signature_def_utils_impl import build_signature_def, predict_signature_def
        from tensorflow.contrib.session_bundle import exporter

        print ("EXPORTING MODEL...")

        export_path = 'exported_brain'
        builder = saved_model_builder.SavedModelBuilder(export_path)

        signature = predict_signature_def(inputs={'inputs': self.brain.keras.input},
                                    outputs={'outputs': self.brain.keras.output})

        with K.get_session() as sess:
            builder.add_meta_graph_and_variables(sess=sess,
                                            tags=[tag_constants.TRAINING],
                                            signature_def_map={'predict': signature})
            builder.save()

        print ("...done!")

    def DumpWeights(self):
        f = open('weights.csv', 'w')
        self.brain.model.dump(f)
        f.close()

    def DumpData(self):
        f = open('traindata.csv', 'w')

        for k,v in self.brain.mapShots.iteritems():
            # Convert our tuple to a numpy array
            if k in self.brain.mapHits:
                a = list(v)
                myList = ','.join(map(str, a))
                output = str(self.brain.mapHits[k])
                f.write(myList+","+output+"\n")

        f.close()  # you can omit in most cases as the destructor will call it
