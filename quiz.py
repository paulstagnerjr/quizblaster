#! python3
from __future__ import division
import numpy as np
import pygame
import math
import csv
import sys
import io
import urllib
from utils import *
from defs import *
from random import randrange
from random import randint
from random import shuffle
import random
from itertools import chain

def truncline(text, font, maxwidth):
    real=len(text)
    stext=text
    l=font.size(text)[0]
    cut=0
    a=0
    done=1
    old = None
    while l > maxwidth:
        a=a+1
        n=text.rsplit(None, a)[0]
        if stext == n:
            cut += 1
            stext= n[:-cut]
        else:
            stext = n
        l=font.size(stext)[0]
        real=len(stext)
        done=0
    return real, done, stext

def wrapline(text, font, maxwidth):
    done=0
    wrapped=[]

    while not done:
        nl, done, stext=truncline(text, font, maxwidth)
        wrapped.append(stext.strip())
        text=text[nl:]
    return wrapped

def wrap_multi_line(text, font, maxwidth):
    """ returns text taking new lines into account.
    """
    lines = chain(*(wrapline(line, font, maxwidth) for line in text.splitlines()))
    return list(lines)

# TODO Make a proper audio management class
pygame.mixer.init()
shootsfx = pygame.mixer.Sound('audio/HeroLaser.wav')
hitsfx = pygame.mixer.Sound('audio/EnemyHit.wav')
enemyshootsfx = pygame.mixer.Sound('audio/EnemyShoot.wav')
explodesfx = pygame.mixer.Sound('audio/ShipExplode.wav')
respawnsfx = pygame.mixer.Sound('audio/Respawn.wav')


def my_shuff(array):
    this_array = array.copy()
    shuffle(this_array)
    return this_array

def hex_to_rgb(hex):
    h = hex.lstrip('#')
    t = tuple(int(h[i:i+2], 16) for i in (0, 2 ,4))
    return t

class text_box(object):
    def __init__(self, screen, text, x_pos, y_pos, font_color, bgcolor, border_color, font_name, font_size):
        super(text_box, self).__init__()
        self.screen = screen
        if sys.version_info[0] < 3:
            self.text = text.decode('utf-8')
        else:
            self.text = text
        self.x_pos = x_pos
        self.y_pos = y_pos
        self.bgcolor = bgcolor
        self.border_color =  border_color
        self.font_color = font_color
        self.font_name = font_name
        self.font_size = font_size
        self.draw_box()

    def size(self):
        return self.font_size

    def draw_box(self):
        self.font = pygame.font.SysFont(self.font_name, self.font_size)
        self.font_surf = self.font.render(self.text, True, self.font_color)
        if self.x_pos == 'center':
            this_x_pos = int(self.screen.get_width()/2 - self.font_surf.get_width() /2)
        elif self.x_pos > 0:
            this_x_pos = self.x_pos
        if self.y_pos == 'center':
            this_y_pos = int(self.screen.get_height()/2 - self.font_surf.get_height() /2)
        elif self.y_pos > 0:
            this_y_pos = self.y_pos
        if (self.font_surf.get_width() + this_x_pos) < (self.screen.get_width() - 5):
            self.rect = pygame.draw.rect(self.screen, self.bgcolor, (this_x_pos - 10, this_y_pos -10, self.font_surf.get_width() + 20, self.font_surf.get_height() + 15), 0)
            self.border_rect = pygame.draw.rect(self.screen, self.border_color, (this_x_pos - 10, this_y_pos -10, self.font_surf.get_width() + 20, self.font_surf.get_height() + 15), 2)
            self.screen.blit(self.font_surf, (this_x_pos, this_y_pos))
        else:
            if self.font_size > 5:
                self.font_size -= 5
            self.draw_box()

    def set_x_pos(self, x_pos):
        self.x_pos = x_pos

    def set_y_pos(self, y_pos):
        self.y_pos = y_pos

class question_box(object):
    def __init__(self, screen, text, x_pos, y_pos, font_color, bgcolor, border_color, font_name, font_size):
        super(question_box, self).__init__()
        self.screen = screen
        #self.text = text
        if sys.version_info[0] < 3:
            self.text = text.decode('utf-8')
        else:
            self.text = text
        self.x_pos = x_pos
        self.y_pos = y_pos
        self.bgcolor = bgcolor
        self.border_color =  border_color
        self.font_color = font_color
        self.font_name = font_name
        self.font_size = font_size
        self.draw_box()

    def size(self):
        return self.font_size

    def draw_box(self):
        self.font = pygame.font.SysFont(self.font_name, self.font_size)
        self.font_surf = self.font.render(self.text, True, self.font_color)
        #endstop = self.x_pos / 5
        if self.x_pos == 'center':
            this_x_pos = int(self.screen.get_width()/2 - self.font_surf.get_width() /2)
        elif self.x_pos > 0:
            this_x_pos = self.x_pos
        if self.y_pos == 'center':
            this_y_pos = int(self.screen.get_height()/2 - self.font_surf.get_height() /2)
        elif self.y_pos > 0:
            this_y_pos = self.y_pos
        endstop = 10
        if (self.font_surf.get_width() + this_x_pos) < self.screen.get_width() - endstop:
            self.rect = pygame.draw.rect(self.screen, self.bgcolor, (this_x_pos - 10, this_y_pos -10, self.font_surf.get_width() + 20, self.font_surf.get_height() + 15), 0)
            self.border_rect = pygame.draw.rect(self.screen, self.border_color, (this_x_pos - 10, this_y_pos -10, self.font_surf.get_width() + 20, self.font_surf.get_height() + 15), 2)
            self.screen.blit(self.font_surf, (this_x_pos, this_y_pos))
        else:
            if this_x_pos > 30:
                #print ('x ' + str(this_x_pos))
                this_x_pos -= 10
                self.draw_box()
            elif self.font_size > 10:
                #print ('size' + str(self.font_size))
                self.font_size -= 2
                self.draw_box()
            else:
                self.draw_box()

    def set_x_pos(self, x_pos):
        self.x_pos = x_pos

    def set_y_pos(self, y_pos):
        self.y_pos = y_pos

class MathQuiz(object):
    def __init__(self, question_difficulty, previous_questions):
        super(MathQuiz, self).__init__()
        self.question_size = NORMAL_FONT_SIZE
        if previous_questions[0] == 'init':
            self.previous_questions = []
        else:
            self.previous_questions = previous_questions
        self.numarray = [0,1,2,3]
        shuffle(self.numarray)
        # The quiz data.
        if question_difficulty < 2:
            self.math_type = 0
            #self.low_rand = 0
            #self.hi_rand = 10
            self.low_rand = self.fib(int(question_difficulty))
            self.hi_rand = self.fib((question_difficulty + 3 * question_difficulty))
        elif question_difficulty < 4:
            self.math_type = 1
            self.low_rand = 0
            self.hi_rand = 10
        elif question_difficulty < 6:
            self.math_type = 2
            self.low_rand = 0
            self.hi_rand = 10
        elif question_difficulty < 8:
            self.math_type = 3
            self.low_rand = 0
            self.hi_rand = 10
        elif question_difficulty < 10:
            self.math_type = randint(0,1)
            self.low_rand = 10
            self.hi_rand = 100
            self.low_rand = question_difficulty
            self.hi_rand = (question_difficulty + 6 * question_difficulty)
        elif question_difficulty < 15:
            self.math_type = randint(0,2)
            self.low_rand = 10
            self.hi_rand = 100
        elif question_difficulty < 20:
            self.math_type = randint(0,3)
            self.low_rand = 10
            self.hi_rand = 100
        elif question_difficulty < 33:
            self.math_type = randint(0,3)
            self.low_rand = question_difficulty
            self.hi_rand = (question_difficulty + 3 * question_difficulty)
        elif question_difficulty < 40:
            self.math_type = randint(0,3)
            self.low_rand = question_difficulty
            self.hi_rand = (question_difficulty + 3 * question_difficulty)
        elif question_difficulty < 60:
            self.math_type = randint(0,3)
            self.low_rand = question_difficulty
            self.hi_rand = (question_difficulty + 3 * question_difficulty)
        elif question_difficulty < 88:
            self.math_type = randint(0,3)
            self.low_rand = question_difficulty
            self.hi_rand = (question_difficulty + 4 * question_difficulty)
        elif question_difficulty < 100:
            self.math_type = randint(0,3)
            self.low_rand = question_difficulty
            self.hi_rand = (question_difficulty + 5 * question_difficulty)
        elif question_difficulty < 150:
            self.math_type = randint(0,3)
            self.math_type = 1
            self.low_rand = question_difficulty
            self.hi_rand = (question_difficulty + 6 * question_difficulty)
        elif question_difficulty < 200:
            self.math_type = randint(0,3)
            self.low_rand = question_difficulty
            self.hi_rand = (question_difficulty + 7 * question_difficulty)
        elif question_difficulty < 333:
            self.math_type = randint(0,3)
            self.low_rand = question_difficulty
            self.hi_rand = (question_difficulty + 10 * question_difficulty)
        else:
            self.math_type = randint(0,3)
            self.low_rand = self.fib(int(question_difficulty))
            self.hi_rand = self.fib((question_difficulty + 3 * question_difficulty))
        self.first_term = randint(self.low_rand,self.hi_rand)
        self.second_term = randint(self.low_rand,self.hi_rand)
        if self.math_type == 0: # addition
            self.correctAnswer = self.first_term + self.second_term
            self.redherring1 = self.correctAnswer
            self.redherring2 = self.correctAnswer
            self.redherring3 = self.correctAnswer
            while self.redherring1 == self.correctAnswer:
                self.redherring1 = self.first_term + self.second_term - randint(self.low_rand,self.hi_rand)
            while self.redherring2 == self.correctAnswer:
                self.redherring2 = self.first_term + self.second_term - randint(self.low_rand,self.hi_rand)
            while self.redherring3 == self.correctAnswer:
                self.redherring3 = self.first_term + self.second_term - randint(self.low_rand,self.hi_rand)
        elif self.math_type == 1: # subtraction
            self.correctAnswer = self.first_term - self.second_term
            self.redherring1 = self.correctAnswer
            self.redherring2 = self.correctAnswer
            self.redherring3 = self.correctAnswer
            while self.redherring1 == self.correctAnswer:
                self.redherring1 = self.first_term - self.second_term - randint(self.low_rand,self.hi_rand)
            while self.redherring2 == self.correctAnswer:
                self.redherring2 = self.first_term - self.second_term + randint(self.low_rand,self.hi_rand)
            while self.redherring3 == self.correctAnswer:
                self.redherring3 = self.first_term - self.second_term - randint(self.low_rand,self.hi_rand)
        elif self.math_type == 2: # multiplication
            self.correctAnswer = self.first_term * self.second_term
            self.redherring1 = self.correctAnswer
            self.redherring2 = self.correctAnswer
            self.redherring3 = self.correctAnswer
            while self.redherring1 == self.correctAnswer:
                self.redherring1 = self.first_term * self.second_term - randint(self.low_rand,self.hi_rand)
            while self.redherring2 == self.correctAnswer:
                self.redherring2 = self.first_term * self.second_term + randint(self.low_rand,self.hi_rand)
            while self.redherring3 == self.correctAnswer:
                self.redherring3 = self.first_term * self.second_term - randint(self.low_rand,self.hi_rand)
        elif self.math_type == 3: # division
            if self.second_term == 0:
                self.second_term = 0.001
            self.correctAnswer = self.first_term / self.second_term
            self.redherring1 = self.correctAnswer
            self.redherring2 = self.correctAnswer
            self.redherring3 = self.correctAnswer
            while self.redherring1 == self.correctAnswer:
                self.redherring1 = self.first_term / self.second_term - randint(self.low_rand,self.hi_rand)
            while self.redherring2 == self.correctAnswer:
               self.redherring2 = self.first_term / self.second_term + randint(self.low_rand,self.hi_rand)
            while self.redherring3 == self.correctAnswer:
                self.redherring3 = self.first_term / self.second_term - randint(self.low_rand,self.hi_rand)
        self.previous_questions.append([self.correctAnswer, self.redherring1, self.redherring2, self.redherring3])

    def check_answer(self, answer):
        if self.numarray[answer - 1] == 0:
            truthy = 'correct'
        else:
            truthy = 'wrong'
        return truthy

    def fib(self, n):
        #print(n)
        a,b = 1,1
        for i in range(n-1):
          a,b = b,a+b
        return a

    def make_quiz(self, screen, first_term, second_term, answer, red_herring1, red_herring2, red_herring3):
        this_array = [answer, red_herring1, red_herring2, red_herring3]
        if   self.math_type == 0: # addition
            this_phrase = str(first_term) + ' + ' + str(second_term) + ' = x?'
        elif self.math_type == 1: # subtraction
            this_phrase = str(first_term) + ' - ' + str(second_term) + ' = x?'
        elif self.math_type == 2: # multiplication
            this_phrase = str(first_term) + ' * ' + str(second_term) + ' = x?'
        elif self.math_type == 3: # division
            this_phrase = str(first_term) + ' / ' + str(second_term) + ' = x?'
        question_2 = 'solve for x'
        this_question = question_box(screen, this_phrase, this_question_x_pos, (X_BOUND / 32 + this_question_y_pos - this_question_y_pos / 2), FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, self.question_size)
        this_question_2 = question_box(screen, question_2, this_question_x_pos, (X_BOUND / 32 + this_question_y_pos + this_question_y_pos / 2), FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, int(self.question_size * 0.8))
        text_box(screen, '1.', this_answer_leader_x_pos, first_answer_y_pos, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        text_box(screen, str(this_array[self.numarray[0]]), this_answer_x_pos, first_answer_y_pos, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        text_box(screen, '2.', this_answer_leader_x_pos, second_answer_y_pos, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        text_box(screen, str(this_array[self.numarray[1]]), this_answer_x_pos, second_answer_y_pos, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        text_box(screen, '3.', this_answer_leader_x_pos, third_answer_y_pos, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        text_box(screen, str(this_array[self.numarray[2]]), this_answer_x_pos, third_answer_y_pos, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        text_box(screen, '4.', this_answer_leader_x_pos, fourth_answer_y_pos, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        text_box(screen, str(this_array[self.numarray[3]]), this_answer_x_pos, fourth_answer_y_pos, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)

    def make_question(self, screen, first_term, second_term, answer, red_herring1, red_herring2, red_herring3):
        this_array = [answer, red_herring1, red_herring2, red_herring3]
        if   self.math_type == 0: # addition
            this_phrase = str(first_term) + ' + ' + str(second_term) + ' = x?'
        elif self.math_type == 1: # subtraction
            this_phrase = str(first_term) + ' - ' + str(second_term) + ' = x?'
        elif self.math_type == 2: # multiplication
            this_phrase = str(first_term) + ' * ' + str(second_term) + ' = x?'
        elif self.math_type == 3: # division
            this_phrase = str(first_term) + ' / ' + str(second_term) + ' = x?'
        question_2 = 'solve for x'
        this_question_line1 = question_box(screen, this_phrase, 'center', (3 * Y_BOUND / 8 ), FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        this_question_line2 = question_box(screen, question_2, 'center', (5 * Y_BOUND / 8),     FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)

    def show_question(self, screen):
        self.make_question(screen, self.first_term, self.second_term, self.correctAnswer, self.redherring1, self.redherring2, self.redherring3)

    def start_quiz(self, screen):
        self.make_quiz(screen, self.first_term, self.second_term, self.correctAnswer, self.redherring1, self.redherring2, self.redherring3)

    def get_previous_questions(self):
        return self.previous_questions

class StateQuiz(object):
    def __init__(self, question, states_left):
        super(StateQuiz, self).__init__()
        # The quiz data. Keys are states and values are their capitals.
        self.capitals = {'Alabama': 'Montgomery', 'Alaska': 'Juneau', 'Arizona': 'Phoenix', 'Arkansas': 'Little Rock', 'California': 'Sacramento', 'Colorado': 'Denver', 'Connecticut': 'Hartford', 'Delaware': 'Dover', 'Florida': 'Tallahassee', 'Georgia': 'Atlanta', 'Hawaii': 'Honolulu', 'Idaho': 'Boise', 'Illinois': 'Springfield', 'Indiana': 'Indianapolis', 'Iowa': 'Des Moines', 'Kansas': 'Topeka', 'Kentucky': 'Frankfort', 'Louisiana': 'Baton Rouge', 'Maine': 'Augusta', 'Maryland': 'Annapolis', 'Massachusetts': 'Boston', 'Michigan': 'Lansing', 'Minnesota': 'Saint Paul', 'Mississippi': 'Jackson', 'Missouri': 'Jefferson City', 'Montana': 'Helena', 'Nebraska': 'Lincoln', 'Nevada': 'Carson City', 'New Hampshire': 'Concord', 'New Jersey': 'Trenton', 'New Mexico': 'Santa Fe', 'New York': 'Albany', 'North Carolina': 'Raleigh', 'North Dakota': 'Bismarck', 'Ohio': 'Columbus', 'Oklahoma': 'Oklahoma City', 'Oregon': 'Salem', 'Pennsylvania': 'Harrisburg', 'Rhode Island': 'Providence', 'South Carolina': 'Columbia', 'South Dakota': 'Pierre', 'Tennessee': 'Nashville', 'Texas': 'Austin', 'Utah': 'Salt Lake City', 'Vermont': 'Montpelier', 'Virginia': 'Richmond', 'Washington': 'Olympia', 'West Virginia': 'Charleston', 'Wisconsin': 'Madison', 'Wyoming': 'Cheyenne'}
        #self.capitals = {'Alabama': 'Montgomery', 'Alaska': 'Juneau', 'Arizona': 'Phoenix', 'Arkansas': 'Little Rock', 'California': 'Sacramento'}
        self.capitalsItems = list(self.capitals.items())
        if states_left[0] == 'init':
            self.states = list(self.capitals.keys()) # get all states in a list
        elif states_left[0] == 'fiver':
            self.states = list(self.capitals.keys()) # get all states in a list
            # remove all but five states in a list
            while len(self.states) > fiver_limit:
                random.shuffle(self.states) # randomize the order of the states
                del self.states[0] # remove a state from the stack
        else:
            self.states = states_left
        # parental
        self.numarray = [0,1,2,3]
        shuffle(self.numarray)
        # Shuffle the order of the states.
        random.shuffle(self.states) # randomize the order of the states
        #print(self.states)
        # Get right and wrong answers.
        self.qstate = self.states[0]
        self.correctAnswer = self.capitals[self.states[0]]
        self.wrongAnswers = list(self.capitals.values()) # get a complete list of answers
        del self.wrongAnswers[self.wrongAnswers.index(self.correctAnswer)] # remove the right answer
        del self.states[0] # remove the state from the stack
        self.wrongAnswers = random.sample(self.wrongAnswers, 3) # pick 3 random ones

    def get_states_left(self):
        return self.states

    def check_answer(self, answer):
        if self.numarray[answer - 1] == 0:
            truthy = 'correct'
        else:
            truthy = 'wrong'
        return truthy

    def make_quiz(self, screen, state, answer, red_herring1, red_herring2, red_herring3):
        this_array = [answer, red_herring1, red_herring2, red_herring3]
        this_question_line1 = question_box(screen, 'What is the capitol of', this_question_x_pos, (X_BOUND / 32 + this_question_y_pos - this_question_y_pos / 2), FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        question_2 = state + '?'
        this_question_line2 = question_box(screen, question_2, this_question_x_pos, (X_BOUND / 32 + this_question_y_pos + this_question_y_pos / 2),FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        # text_box(screen, '1.', 230, 300, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        # text_box(screen, this_array[self.numarray[0]], 350, 300, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        # text_box(screen, '2.', 230, 400, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        # text_box(screen, this_array[self.numarray[1]], 350, 400, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        # text_box(screen, '3.', 230, 500, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        # text_box(screen, this_array[self.numarray[2]], 350, 500, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        # text_box(screen, '4.', 230, 600, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        # text_box(screen, this_array[self.numarray[3]], 350, 600, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        text_box(screen, '1.', this_answer_leader_x_pos, first_answer_y_pos, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        text_box(screen, str(this_array[self.numarray[0]]), this_answer_x_pos, first_answer_y_pos, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        text_box(screen, '2.', this_answer_leader_x_pos, second_answer_y_pos, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        text_box(screen, str(this_array[self.numarray[1]]), this_answer_x_pos, second_answer_y_pos, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        text_box(screen, '3.', this_answer_leader_x_pos, third_answer_y_pos, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        text_box(screen, str(this_array[self.numarray[2]]), this_answer_x_pos, third_answer_y_pos, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        text_box(screen, '4.', this_answer_leader_x_pos, fourth_answer_y_pos, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        text_box(screen, str(this_array[self.numarray[3]]), this_answer_x_pos, fourth_answer_y_pos, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)

    def show_question(self, screen):
        this_question_line1 = question_box(screen, 'What is the capitol of', 'center', (3 * Y_BOUND / 8 ), FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        question_2 = self.qstate + '?'
        this_question_line2 = question_box(screen, question_2, 'center', (5 * Y_BOUND / 8),     FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)

    def start_quiz(self, screen):
        self.make_quiz(screen, self.qstate, self.correctAnswer, self.wrongAnswers[0], self.wrongAnswers[1], self.wrongAnswers[2])

class OpenTD_Quiz(object):
    def __init__(self):
        super(OpenTD_Quiz, self).__init__()
        self.numarray = [0,1,2,3]
        shuffle(self.numarray)
        self.questionList = self.read_in_questions('opentdb.tsv')
        self.get_next_question()
        self.question_size = NORMAL_FONT_SIZE
        self.font = pygame.font.SysFont(NORMAL_FONT, NORMAL_FONT_SIZE)

    def read_in_questions(self, this_file):
        # Read in questions
        this_questionList = []
        count = 0
        #with open(this_file, newline='', encoding='utf-8') as csvfile:
        if sys.version_info[0] < 3:
            with open(this_file, 'rb') as csvfile:
              reader = csv.DictReader(csvfile, delimiter='\t', quotechar='"')
              #reader = UnicodeDictReader(csvfile)
              for row in reader:
                this_questionList.append(row)
                count += 1
            shuffle(this_questionList)
            return this_questionList
        else:
            with open(this_file, encoding='utf-8', newline='') as csvfile:
              reader = csv.DictReader(csvfile, delimiter='\t', quotechar='"')
              #reader = UnicodeDictReader(csvfile)
              for row in reader:
                this_questionList.append(row)
                count += 1
            shuffle(this_questionList)
            return this_questionList

    def check_answer(self, answer):
        #print(self.numarray)
        if self.numarray[answer - 1] == 0:
            truthy = 'correct'
        else:
            truthy = 'wrong'
        return truthy

    def make_quiz(self, screen):
        this_array = [self.answer, self.red_herring1, self.red_herring2, self.red_herring3]
        my_list = wrapline(self.question, self.font, 1000)
        if len(my_list) == 1:
            this_question = question_box(screen, self.question, this_question_x_pos, this_question_y_pos, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, self.question_size)
        elif len(my_list) == 2:
            this_question = question_box(screen, my_list[0], this_question_x_pos, (X_BOUND / 32 + this_question_y_pos - this_question_y_pos / 2), FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, self.question_size)
            this_question_2 = question_box(screen, my_list[1],this_question_x_pos, (X_BOUND / 32 + this_question_y_pos + this_question_y_pos / 2), FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, self.question_size)
        elif len(my_list) > 2:
            this_question_1_text = my_list.pop(0)
            this_question_1_text += ' '
            this_question_1_text += my_list.pop(0)
            this_question = question_box(screen, this_question_1_text, this_question_x_pos, (X_BOUND / 32 + this_question_y_pos - this_question_y_pos / 2), FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, self.question_size)
            this_question_2_text = my_list.pop(0)
            for line in  my_list:
                this_question_2_text += ' '
                this_question_2_text += line
            this_question_2 = question_box(screen, this_question_2_text, this_question_x_pos, (X_BOUND / 32 + this_question_y_pos + this_question_y_pos / 2), FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, self.question_size)
            #this_question = question_box(screen, question, this_question_x_pos, (X_BOUND / 32 + this_question_y_pos - this_question_y_pos / 2), FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, self.question_size)
            #this_question_2 = question_box(screen, question_2, this_question_x_pos, (X_BOUND / 32 + this_question_y_pos + this_question_y_pos / 2), FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, self.question_size)
        else:
            print('list out of bounds')
        #this_question = question_box(screen, self.question, this_question_x_pos, this_question_y_pos, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, self.question_size)
        self.question_size = this_question.size()
        text_box(screen, '1.', this_answer_leader_x_pos, first_answer_y_pos, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        text_box(screen, str(this_array[self.numarray[0]]), this_answer_x_pos, first_answer_y_pos, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        text_box(screen, '2.', this_answer_leader_x_pos, second_answer_y_pos, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        text_box(screen, str(this_array[self.numarray[1]]), this_answer_x_pos, second_answer_y_pos, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        text_box(screen, '3.', this_answer_leader_x_pos, third_answer_y_pos, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        text_box(screen, str(this_array[self.numarray[2]]), this_answer_x_pos, third_answer_y_pos, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        text_box(screen, '4.', this_answer_leader_x_pos, fourth_answer_y_pos, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        text_box(screen, str(this_array[self.numarray[3]]), this_answer_x_pos, fourth_answer_y_pos, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)

    def show_question(self, screen):
        my_list = wrapline(self.question, self.font, 1000)
        if len(my_list) == 1:
            this_question   = question_box(screen, self.question, 'center', 'center',       FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, self.question_size)
        elif len(my_list) == 2:
            this_question   = question_box(screen, my_list[0], 'center', (3 * Y_BOUND / 8 ),    FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, self.question_size)
            this_question_2 = question_box(screen, my_list[1], 'center', (5 * Y_BOUND / 8), FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, self.question_size)
        elif len(my_list) == 3:
            this_question   = question_box(screen, my_list[0], 'center', (Y_BOUND / 4),     FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, self.question_size)
            this_question_2 = question_box(screen, my_list[1], 'center', (Y_BOUND / 2),     FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, self.question_size)
            this_question_3 = question_box(screen, my_list[2], 'center', (3 * Y_BOUND / 4), FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, self.question_size)
        elif len(my_list) == 4:
            this_question   = question_box(screen, my_list[0], 'center', (Y_BOUND / 10),     FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, self.question_size)
            this_question_2 = question_box(screen, my_list[1], 'center', (Y_BOUND / 3),     FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, self.question_size)
            this_question_3 = question_box(screen, my_list[2], 'center', (Y_BOUND - Y_BOUND / 3),     FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, self.question_size)
            this_question_4 = question_box(screen, my_list[3], 'center', (Y_BOUND - Y_BOUND / 10), FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, self.question_size)
        elif len(my_list) > 4:
            this_question_1_text = my_list.pop(0)
            this_question_2_text = my_list.pop(0)
            this_question_3_text = my_list.pop(0)
            this_question_4_text = my_list.pop(0)
            for line in  my_list:
                this_question_4_text += ' '
                this_question_4_text += line
            this_question   = question_box(screen, this_question_1_text, 'center', (Y_BOUND / 10),           FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, self.question_size)
            this_question_2 = question_box(screen, this_question_2_text, 'center', (Y_BOUND / 3),            FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, self.question_size)
            this_question_3 = question_box(screen, this_question_3_text, 'center', (Y_BOUND - Y_BOUND / 3),  FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, self.question_size)
            this_question_4 = question_box(screen, this_question_4_text, 'center', (Y_BOUND - Y_BOUND / 10), FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, self.question_size)
        else:
            print('list out of bounds')

    def get_next_question(self):
        if len(self.questionList) > 0:
            this_quiz = self.questionList.pop()
            self.question = urllib.parse.unquote(this_quiz['Question'])
            self.answer = urllib.parse.unquote(this_quiz['Answer'])
            self.red_herring1 = urllib.parse.unquote(this_quiz['RedHerring1'])
            self.red_herring2 = urllib.parse.unquote(this_quiz['RedHerring2'])
            self.red_herring3 = urllib.parse.unquote(this_quiz['RedHerring3'])
            self.question_size = NORMAL_FONT_SIZE
            shuffle(self.numarray)
            return True
        else:
            return False

    def wrongfx(self):
            wrongfx.play()

    def get_row_spec(self, row_num, quizboxnum, quiztype):
        count = 0
        for row in self.questionList:
            if row['QuizBoxNum'] == str(quizboxnum):
                if row['QuizType'] == str(quiztype):
                    if count == row_num:
                        return row
                    count += 1

    def get_num_rows_spec(self, quizboxnum, quiztype):
        count = 0
        for row in self.questionList:
            if row['QuizBoxNum'] == str(quizboxnum):
                if row['QuizType'] == quiztype:
                    count += 1
        return count

    def get_num_rows(self):
        count = 0
        for row in self.questionList:
            count += 1
        return count

    def draw_quiz(self, screen):
        self.make_quiz(screen)

    def start_quiz_spec(self, screen, quizboxnum, quiztype):
        self.num_rows = self.get_num_rows_spec(quizboxnum, quiztype)
        this_quiz = self.get_row_spec(self.quiz_num, quizboxnum, quiztype)
        self.make_quiz(screen, this_quiz['QuizBoxNum'], this_quiz['QuizType'], this_quiz['Question'], this_quiz['Answer'], this_quiz['RedHerring1'], this_quiz['RedHerring2'], this_quiz['RedHerring3'])

    def set_quiz(self, quiz_num):
        self.quiz_num = quiz_num

class QuizBlaster(object):
    def __init__(self, questionList):
        super(QuizBlaster, self).__init__()
        self.numarray = [0,1,2,3]
        shuffle(self.numarray)
        self.question_size = NORMAL_FONT_SIZE
        if questionList == 0:
          #print('read question list')
          self.questionList = self.read_in_questions('questions.tsv')
        else:
          #print('existing question list')
          self.questionList = questionList
          #print(self.questionList)
        self.num_rows = self.get_num_rows()
        self.font = pygame.font.SysFont(NORMAL_FONT, NORMAL_FONT_SIZE)
        self.set_quiz(0)

    def return_questions(self):
          return self.questionList

    def read_in_questions(self, this_file):
        # Read in questions
        this_questionList = []
        count = 0
        if sys.version_info[0] < 3:
            with open(this_file, 'rb') as csvfile:
              reader = csv.DictReader(csvfile, delimiter='\t', quotechar='"')
              #reader = UnicodeDictReader(csvfile)
              for row in reader:
                this_questionList.append(row)
                count += 1
            shuffle(this_questionList)
            return this_questionList
        else:
            with open(this_file, encoding='utf-8', newline='') as csvfile:
              reader = csv.DictReader(csvfile, delimiter='\t', quotechar='"')
              #reader = UnicodeDictReader(csvfile)
              for row in reader:
                this_questionList.append(row)
                count += 1
            shuffle(this_questionList)
            return this_questionList

    def check_answer(self, answer):
        if self.numarray[answer - 1] == 0:
            truthy = 'correct'
        else:
            truthy = 'wrong'
        return truthy

    def make_quiz(self, screen, quizboxnum, quiztype, question, question_2, answer, red_herring1, red_herring2, red_herring3, randLL, randLH, randHL, randHH):
        this_array = [answer, red_herring1, red_herring2, red_herring3]
        if question_2:
            this_question = question_box(screen, question, this_question_x_pos, (X_BOUND / 32 + this_question_y_pos - this_question_y_pos / 2), FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, self.question_size)
            this_question_2 = question_box(screen, question_2, this_question_x_pos, (X_BOUND / 32 + this_question_y_pos + this_question_y_pos / 2), FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, self.question_size)
        else:
            this_question = question_box(screen, question, this_question_x_pos, this_question_y_pos, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, self.question_size)
        self.question_size = this_question.size()
        text_box(screen, '1.', this_answer_leader_x_pos, first_answer_y_pos, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        text_box(screen, this_array[self.numarray[0]], this_answer_x_pos, first_answer_y_pos, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        text_box(screen, '2.', this_answer_leader_x_pos, second_answer_y_pos, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        text_box(screen, this_array[self.numarray[1]], this_answer_x_pos, second_answer_y_pos, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        text_box(screen, '3.', this_answer_leader_x_pos, third_answer_y_pos, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        text_box(screen, this_array[self.numarray[2]], this_answer_x_pos, third_answer_y_pos, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        text_box(screen, '4.', this_answer_leader_x_pos, fourth_answer_y_pos, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)
        text_box(screen, this_array[self.numarray[3]], this_answer_x_pos, fourth_answer_y_pos, FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, NORMAL_FONT_SIZE)

    def show_question(self, screen, quizboxnum, quiztype):
        self.num_rows = self.get_num_rows_spec(quizboxnum, quiztype)
        this_quiz = self.get_row_spec(self.quiz_num, quizboxnum, quiztype)
        my_list = wrapline(this_quiz['Question'], self.font, 1000)
        if this_quiz['Question2']:
            this_question   = question_box(screen, this_quiz['Question'],  'center', (3 * Y_BOUND / 8), FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, self.question_size)
            this_question_2 = question_box(screen, this_quiz['Question2'], 'center', (5 * Y_BOUND / 8), FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, self.question_size)
        else:
            this_question   = question_box(screen, this_quiz['Question'],  'center', 'center', FONT_COLOR,BOX_COLOR,BORDER_COLOR, MENU_FONT, self.question_size)

    def get_row(self, row_num):
        count = 0
        for row in self.questionList:
            if count == row_num:
                return row
            count += 1

    def get_row_spec(self, row_num, quizboxnum, quiztype):
        count = 0
        for row in self.questionList:
            if row['QuizBoxNum'] == str(quizboxnum):
                if row['QuizType'] == str(quiztype):
                    if count == row_num:
                        return row
                    count += 1

    def delete_question(self, row_num, quizboxnum, quiztype):
        count = 0
        over_count = 0
        for row in self.questionList:
            if row['QuizBoxNum'] == str(quizboxnum):
                if row['QuizType'] == str(quiztype):
                    if count == row_num:
                        del self.questionList[over_count]
                    count += 1
            over_count += 1

    def get_num_rows_spec(self, quizboxnum, quiztype):
        count = 0
        for row in self.questionList:
            if row['QuizBoxNum'] == str(quizboxnum):
                if row['QuizType'] == quiztype:
                    count += 1
        return count

    def get_num_rows(self):
        count = 0
        for row in self.questionList:
            count += 1
        return count

    def start_quiz(self, screen):
        this_quiz = self.get_row(self.quiz_num)
        self.make_quiz(screen, this_quiz['QuizBoxNum'], this_quiz['QuizType'], this_quiz['Question'], this_quiz['Question2'], this_quiz['Answer'], this_quiz['RedHerring1'], this_quiz['RedHerring2'], this_quiz['RedHerring3'], this_quiz['randLL'], this_quiz['randLH'], this_quiz['randHL'], this_quiz['randHH'])

    def start_quiz_spec(self, screen, quizboxnum, quiztype):
        self.num_rows = self.get_num_rows_spec(quizboxnum, quiztype)
        this_quiz = self.get_row_spec(self.quiz_num, quizboxnum, quiztype)
        self.make_quiz(screen, this_quiz['QuizBoxNum'], this_quiz['QuizType'], this_quiz['Question'], this_quiz['Question2'], this_quiz['Answer'], this_quiz['RedHerring1'], this_quiz['RedHerring2'], this_quiz['RedHerring3'], this_quiz['randLL'], this_quiz['randLH'], this_quiz['randHL'], this_quiz['randHH'])

    def set_quiz(self, quiz_num):
        self.quiz_num = quiz_num
