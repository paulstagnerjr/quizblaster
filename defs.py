#from quiz import *
import socket
import random
import pygame

def hex_to_rgb(hex):
    h = hex.lstrip('#')
    t = tuple(int(h[i:i+2], 16) for i in (0, 2 ,4))
    return t
#import quiz

FPS = 30  # can lower this to reduce system resource usage
FADE_RATE = 2 # lower values mean fireworks fade out more slowly
# controls
FIREWORK_MOUSE_BUTTON = 1  # left click
SHIMMER_MOUSE_BUTTON = 3   # right click

# Define some colors
BLACK = (0, 0, 0)
BLACK_FADED = [0, 0, 0, FADE_RATE]
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
#RED    = quiz.hex_to_rgb('#cc2729')
#BLUE   = quiz.hex_to_rgb('#0000b1')
#YELLOW = quiz.hex_to_rgb('#f8ee3f')
RED    = hex_to_rgb('#cc2729')
BLUE   = hex_to_rgb('#0000b1')
LIGHT_BLUE   = hex_to_rgb('#2222b1')
YELLOW = hex_to_rgb('#f8ee3f')
FONT_COLOR = YELLOW
BOX_COLOR = BLACK
BORDER_COLOR = RED
BG_COLOR = BLACK

if socket.gethostname() == 'flux.thalhalla.net':
    #print('flux.thalhalla.net')
    #X_BOUND = random.randint(800,1920)
    #Y_BOUND = int(9 * X_BOUND / 16)
    X_BOUND = 800
    Y_BOUND = 480
elif socket.gethostname() == 'quizbox101':
    X_BOUND = 800
    Y_BOUND = 480
elif socket.gethostname() == 'quizbox102':
    X_BOUND = 800
    Y_BOUND = 480
elif socket.gethostname() == 'quizbox103':
    X_BOUND = 800
    Y_BOUND = 480
elif socket.gethostname() == 'quizbox201':
    #print('quizbox2')
    X_BOUND = 800
    Y_BOUND = 480
elif socket.gethostname() == 'quizbox202':
    #print('quizbox2')
    X_BOUND = 800
    Y_BOUND = 480
else:
    #print('unknown hostname')
    X_BOUND = 800
    Y_BOUND = 480
SCORE_POSITION = X_BOUND / 18
FPS_POSITION = X_BOUND / 2
HEALTH_POSITION = X_BOUND  - ( X_BOUND / 4 )
LIVES_POSITION = X_BOUND - X_BOUND /10
TOP_BAR = 6
MENU_FONT = 'DejaVu'
NORMAL_FONT = 'DejaVu'
MENU_FONT_SIZE = int(X_BOUND / 32)
HUGE_FONT_SIZE = int(X_BOUND / 8)
NORMAL_FONT_SIZE = int(X_BOUND / 16)
LEADERBOARD_FONT_SIZE = int(X_BOUND / 23)

# Configure screen TODO: Should there be a config object or something to contain this?
resolution_x = X_BOUND
resolution_y = Y_BOUND
resolution = (resolution_x, resolution_y)
CORRECT_ANSWER_FIREWORKS = 1
GAMEWIN_FIREWORKS = 3
CORRECT_ANSWER_THRESH = 1
WRONG_ANSWER_THRESH = 3
GAMEWIN_THRESH = 6
GAMEOVER_THRESH = 8


# Acceleration in pixels per second
SHIP_ACC = 100
DEADZONE = 0.25

## Constants
MAX_STARS = 50

# Game vars
MAX_LIVES = 3
END_LIVES = 1
MAX_HEALTH = 999
HEALTH_POTION = 75
WRONG_HIT = 450
QUESTION_TIMEOUT = 20

# Quiz positions
#this_question_x_pos = int(X_BOUND / 64)
this_question_x_pos = 'center'
this_question_y_pos = int(Y_BOUND / 9)
this_answer_leader_x_pos = int(X_BOUND / 64)
#this_answer_leader_x_pos = int(X_BOUND / 3)
#this_answer_x_pos = int(X_BOUND / 9)
this_answer_x_pos = 'center'
first_answer_y_pos = int(Y_BOUND - 4 * (Y_BOUND / 6))
second_answer_y_pos = int(Y_BOUND - 3 * (Y_BOUND / 6))
third_answer_y_pos = int(Y_BOUND - 2 * (Y_BOUND / 6))
fourth_answer_y_pos = int(Y_BOUND - Y_BOUND / 6)
leaderboard_y_inc = Y_BOUND /20
y_bottom = int(Y_BOUND - Y_BOUND /16)
left_quarter=int(X_BOUND/5)
right_quarter=int(X_BOUND - X_BOUND/3)

fiver_limit = 5
#main_logo = pygame.image.load("images/redbull_challenge522.png")
main_logo = pygame.image.load("images/start_screen.png")
redbull_splash = pygame.image.load("images/RedBull.png")
game_over_screen = pygame.image.load("images/game_over_screen.png")
MENU_OSC_TIME = 8
