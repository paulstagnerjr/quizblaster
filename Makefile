run:
	#python3 ./game.py -v -s
	#python ./game.py -v -s
	python ./game.py -v

full:
	#python3 ./game.py -v -f
	python ./game.py -v -f

pireqs:
	sudo apt install libatlas-base-dev libsdl1.2-dev xorg byobu vim fail2ban git unclutter mongodb \
	  libsdl-dev libsdl-image1.2-dev libsdl-mixer1.2-dev libsdl-ttf2.0-dev \
	  libsmpeg-dev libportmidi-dev libavformat-dev libswscale-dev python3-pip
	pip3 install --user tensorflow pygame pymongo numpy keras

opencv-reqs:
	sudo apt-get update && sudo apt-get upgrade
	sudo apt-get install  \
	build-essential cmake pkg-config \
	libjpeg-dev libtiff5-dev libjasper-dev libpng-dev \
	libavcodec-dev libavformat-dev libswscale-dev libv4l-dev \
	libxvidcore-dev libx264-dev \
	libgtk2.0-dev libgtk-3-dev libatlas-base-dev gfortran \
	python2.7-dev python3-dev python-pip libpt-dev
	sudo -H pip install virtualenv virtualenvwrapper
	sudo -H pip3 install virtualenv virtualenvwrapper
	sudo rm -rf ~/.cache/pip
	echo -e "\n# virtualenv and virtualenvwrapper" >> ~/.profile
	echo "export WORKON_HOME=$HOME/.virtualenvs" >> ~/.profile
	echo "export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3" >> ~/.profile
	echo "source /usr/local/bin/virtualenvwrapper.sh" >> ~/.profile
	mkvirtualenv cv -p python3

opencv:
	git clone https://github.com/opencv/opencv.git
	cd opencv; git checkout 3.4.2

opencv_contrib:
	git clone https://github.com/opencv/opencv_contrib.git
	cd opencv_contrib; git checkout 3.4.2

rootreqs:
	sudo -H pip3 install tensorflow pygame pymongo numpy keras opencv

reqs:
	sudo apt install libatlas-base-dev
	sudo -H pip3 install --user -r requirements.txt

sync:
	$(eval PI_SRV := $(shell cat .pi_srv))
	cd ..; rsync -avz --exclude 'quizblaster/__pycache__'  --exclude 'quizblaster/.git' quizblaster pi@${PI_SRV}:/home/pi/

sync2:
	$(eval PI_SRV2 := $(shell cat .pi_srv2))
	cd ..; rsync -avz  --exclude 'quizblaster/.git' quizblaster pi@${PI_SRV2}:/home/pi/

db: /tmp/mongo-stdout.log tail

tail:
	tail -f /tmp/mongo*

/tmp/mongo-stdout.log:
	mongod --dbpath ./db 1>/tmp/mongo-stdout.log 2>/tmp/mongo-err.log &
