import pygame
from static import tools

class ltline:
    def __init__(self, screen, vectorA, vectorB, thickness):
        super(ltline, self).__init__()
        self.screen = screen
        self.vectorA = vectorA
        self.vectorB = vectorB
        self.thickness = thickness
        self.lt_endcap = pygame.image.load("images/lightning-endcap.png")
        self.lt_mid = pygame.image.load("images/lightning-mid.png")

    def draw(self, spritebatch, color):
        self.vectorR = vectorA - vectorB
        rotation = atan2(atan(vectorR.y), atan(vectorR.x)
        ImageThickness = 8
        thicknessScale = self.thickness / ImageThickness
        capOrigin = vector(self.lt_endcap_x = self.lt_endcap.get_width(),self.lt_endcap_x = self.lt_endcap.get_height() /2)
        middleOrigin = vector(0,self.lt_endcap_x = self.lt_endcap.get_height() /2)
        middleScale = vector(vectorR.length, thicknessScale)

        #fail spritebatch.draw(self.lt_mid, vectorA, null, color, rotation, middleOrigin, middleScale, SpriteEffects???)

class SpriteBatch():
    def __init__(self):
	self.draw=set()

    def add(self,drawAble):
        a=(drawAble.zIndex,drawAble)
        self.draw.add(a)
        print(self.draw)

    def remove(self,drawAble):
        a=drawAble.zIndex,drawAble
        self.draw.remove(a)

    def drawNow(self):
	c=tools.camera
	screen=tools.screen
	for i in self.draw:
	    #change i to the drawAbleObject
	    i=i[1]
	    pos=i.position[0]-c[0],i.position[1]-c[1]
	    screen.blit(i.image,pos)
