import sys
from xdo import Xdo
from time import sleep

def sendkeys(*keys):
    for k in keys: xdo.send_keysequence_window(0, k.encode())

def type(text):
    xdo.enter_text_window(0, text.encode())

#sleep(0.5)
xdo = Xdo()

#sendkeys('Tab', 'Up', 'ctrl+1', 'Down', 'ctrl+v', 'Right')
sendkeys('9')
sleep(0.5)
sendkeys('1', '2')
sleep(0.5)
sendkeys('9')
sleep(0.5)
sendkeys('2')
sleep(0.5)
sendkeys('9')
sleep(0.5)
sendkeys('1')
