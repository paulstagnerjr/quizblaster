from __future__ import print_function
import sys

def ep(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)
