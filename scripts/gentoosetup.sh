#!/bin/bash
set -ex
if [ $# -ne 2 ]; then
  # Print usage
  echo 'Error! wrong number of arguments'
  echo 'usage:'
  echo 'gentoosetup.sh DISTCC_HOST DISTCC_JOBS'
  exit 1
fi
DISTCC_HOST=$1
DISTCC_JOBS=$2

main () {
  emerge-webrsync
  emerge --oneshot portage
  genup
  echo "sys-devel/gcc -~arm64" >> /etc/portage/package.accept_keywords/gcc
  emerge -u sys-devel/gcc
  gcc-config --list-profiles
  gcc-config 1
  env-update && source /etc/profile
  emerge --oneshot sys-devel/libtool
  emerge --ask --verbose sys-devel/distcc
  echo "$DISTCC_HOST/$DISTCC_JOBS,cpp,lzo" >> /etc/distcc/hosts
}

time main $@
