#!/bin/bash
SLEEP_TIME=1
THIS_CWD=$(pwd)
THIS_URL='https://opentdb.com/api.php?amount=50&type=multiple&encode=url3986'
THIS_BOOL_URL='https://opentdb.com/api.php?amount=50&type=boolean&encode=url3986'
grabbools () {
  for i in {1..$1}; do sleep $SLEEP_TIME; wget -q "$THIS_BOOL_URL" -O $TMP/${i}-rand-nocat.json; done
}

grabs () {
#  for i in {1..32}; do sleep $SLEEP_TIME; wget "https://opentdb.com/api.php?amount=50&category=${i}&difficulty=easy&type=multiple" -O $TMP/${i}-easy.json; done
#  for i in {1..32}; do sleep $SLEEP_TIME; wget "https://opentdb.com/api.php?amount=50&category=${i}&difficulty=medium&type=multiple" -O $TMP/${i}-medium.json; done
#  for i in {1..32}; do sleep $SLEEP_TIME; wget "https://opentdb.com/api.php?amount=50&category=${i}&difficulty=hard&type=multiple" -O $TMP/${i}-hard.json; done
#  for i in {1..32}; do sleep $SLEEP_TIME; wget "https://opentdb.com/api.php?amount=50&category=${i}&difficulty=easy&type=boolean" -O $TMP/${i}-easy-bool.json; done
#  for i in {1..32}; do sleep $SLEEP_TIME; wget "https://opentdb.com/api.php?amount=50&category=${i}&difficulty=medium&type=boolean" -O $TMP/${i}-medium.-booljson; done
#  for i in {1..32}; do sleep $SLEEP_TIME; wget "https://opentdb.com/api.php?amount=50&category=${i}&difficulty=hard&type=boolean" -O $TMP/${i}-hard.json-bool; done
#  for i in {1..32}; do sleep $SLEEP_TIME; wget "https://opentdb.com/api.php?amount=50&category=${i}&type=multiple" -O $TMP/${i}-rand.json; done
  #for i in {1..4}; do sleep $SLEEP_TIME; wget -q "https://opentdb.com/api.php?amount=50&&type=multiple" -O $TMP/${i}-rand-nocat.json; done
  for i in {1..$1}; do sleep $SLEEP_TIME; wget -q "$THIS_URL" -O $TMP/${i}-rand-nocat.json; done
}

mmmktsv () {
  this_json=$1
  jq -r \
    '.results[] | "1xxtabxxVisualxxtabxx\(.category)xxtabxx\(.type)xxtabxx\(.difficulty)xxtabxx\(.question)xxtabxx\(.correct_answer)xxtabxx\(.incorrect_answers)"' \
    $this_json | sed 's/xxtabxx/\t/g' \
    |sed 's/\[//' \
    |sed 's/\]//' \
    |sed 's/,/\t/g'
}

mktsv () {
  this_json=$1
  jq -r \
    '.results[] | "1xxtabxxVisualxxtabxx\(.category)xxtabxx\(.type)xxtabxx\(.difficulty)xxtabxx\(.question)xxtabxx\(.correct_answer)xxtabxx\(.incorrect_answers[0])xxtabxx\(.incorrect_answers[1])xxtabxx\(.incorrect_answers[2])"' \
    $this_json | sed 's/xxtabxx/\t/g' \
    |sed 's/\[//' \
    |sed 's/\]//'

}

mktsvbool () {
  this_json=$1
  jq -r \
    '.results[] | "1xxtabxxVisualxxtabxx\(.category)xxtabxx\(.type)xxtabxx\(.difficulty)xxtabxx\(.question)xxtabxx\(.correct_answer)xxtabxx\(.incorrect_answers[0])"' \
    $this_json | sed 's/xxtabxx/\t/g' \
    |sed 's/\[//' \
    |sed 's/\]//'
}

booleanquiz () {
  TMP=$(mktemp -d)
  OLDL=$(wc -l $THIS_CWD/opentdb-bool.tsv|awk '{print $1}')
  grabbools $1
  cd $TMP
  tail -n +2 $THIS_CWD/opentdb-bool.tsv >> $TMP/tmp.tsv
  for i in $(ls *.json); do
    mktsv $i >> $TMP/tmp.tsv
  done
  cat $TMP/tmp.tsv | sort | uniq > $TMP/uniq.tsv
  cat $THIS_CWD/head-opentdb-bool.tsv $TMP/uniq.tsv > $THIS_CWD/opentdb-bool.tsv
  NEWL=$(wc -l $THIS_CWD/opentdb-bool.tsv|awk '{print $1}')
  (( DIFFL = NEWL - OLDL ))
  echo "we've gained $DIFFL bool questions, for a total of $NEWL"
  rm  -Rf $TMP
}

multichoice () {
  TMP=$(mktemp -d)
  OLDL=$(wc -l $THIS_CWD/opentdb.tsv|awk '{print $1}')
  grabs $1
  cd $TMP
  tail -n +2 $THIS_CWD/opentdb.tsv >> $TMP/tmp.tsv
  for i in $(ls *.json); do
    mktsv $i >> $TMP/tmp.tsv
  done
  cat $TMP/tmp.tsv | sort | uniq > $TMP/uniq.tsv
  cat $THIS_CWD/head-opentdb.tsv $TMP/uniq.tsv > $THIS_CWD/opentdb.tsv
  NEWL=$(wc -l $THIS_CWD/opentdb.tsv|awk '{print $1}')
  (( DIFFL = NEWL - OLDL ))
  echo "we've gained $DIFFL questions, for a total of $NEWL"
  rm  -Rf $TMP
}

main () {
: ${1:=4}

echo $1
  multichoice $1
  booleanquiz $1
}

time main $@
