FROM ubuntu:bionic
LABEL maintainer "Josh Cox <josh@webhosting.coop>"

ENV \
  BUILD_PACKAGES='sudo make automake libtool pkg-config python-dev' \
  KEEP_PACKAGES='python3-pip curl git ' \
  BASE_PKG='ca-certificates apt-transport-https software-properties-common mongodb-clients' \
  SUDO_FORCE_REMOVE=yes \
  GYMONGO_PYTHON=1 \
  GYMONGONASIUM_UPDATED=20171112

COPY . /build

RUN DEBIAN_FRONTEND=noninteractive \
  && apt-get -qq update && apt-get -qqy dist-upgrade \
  && apt-get -qqy --no-install-recommends install \
     $BUILD_PACKAGES \
     $KEEP_PACKAGES \
     $BASE_PKG \
  && echo '%sudo ALL=(ALL) NOPASSWD:ALL'>> /etc/sudoers \
  && curl -s https://packagecloud.io/install/repositories/akopytov/sysbench/script.deb.sh | sudo bash \
  && sudo apt -y install sysbench \
  && sudo apt -y remove sysbench \
  && cd /build  \
  && pip3 install -r requirements.txt \
  && apt-get -qqy remove \
     $BUILD_PACKAGES \
  && apt-get -y autoremove \
  && apt-get clean \
  && rm -Rf /var/lib/apt/lists/*
