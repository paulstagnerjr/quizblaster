'''The MIT License (MIT)

Copyright (c) 2017 ActiveState Software Inc.

Written by Pete Garcin @rawktron

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.'''

from pymongo import MongoClient
from pymongo import DESCENDING
from datetime import datetime
import pygame
from utils import *
import sys


client = MongoClient('mongodb://localhost:27017/?socketTimeoutMS=500&connectTimeoutMS=500&serverSelectionTimeoutMS=1000')
db = client.test
scores = db.scores

def DisplayLeaderBoard(screen, highscores, this_grandtotal, name):
    y = 100
    displaytext("Grand Total   " + str(this_grandtotal), LEADERBOARD_FONT_SIZE, 'center', 5, YELLOW, screen)
    displaytext("TOP PLAYERS", LEADERBOARD_FONT_SIZE, 'center', 50, YELLOW, screen)
    for s in highscores:
        if s['name'] == name:
            color = WHITE
        else:
            color = YELLOW
        displaytext(s['name'], LEADERBOARD_FONT_SIZE, left_quarter, y, color, screen)
        displaytext(str(s['score']), LEADERBOARD_FONT_SIZE, right_quarter, y, color, screen)
        y+=leaderboard_y_inc

    displaytext("PRESS ANY KEY TO CONTINUE", LEADERBOARD_FONT_SIZE, 'center', y_bottom, YELLOW, screen)


# TODO: Check if a player score exists, and instead add another entry instead of
# overwriting.
def StoreScore(name,score):
    if client == 'mongo unavailable':
        return False
    try:
        print (client)
        print ("Storing "+name+" score: "+str(score))
        scores.insert({"name":name,"score":score,"time":str(datetime.now())})
    except:
        print ('mongo unavailable')
        return False
    return True

def GetScores():
    if client == 'mongo unavailable':
        return ['data_unavailable',0,0]
    try:
        return list(scores.find().sort("score",DESCENDING).limit(10))
    except:
        print ('mongo unavailable')
        return False

def GetGrandTotal():
    if client == 'mongo unavailable':
        return ['data_unavailable',0,0]
    try:
        cumulative_total = 0
        all_highscores = list(scores.find().sort("score",DESCENDING))
        for s in all_highscores:
          cumulative_total += int(s['score'])
        return cumulative_total
    except:
        print ('mongo unavailable')
        return False

def ClearScores():
    if client == 'mongo unavailable':
        return False
    try:
        scores.remove({})
    except:
        print ('mongo unavailable')
        return False
    return True

def Test():
    # Clear scores first for testing
    if client == 'mongo unavailable':
        return
    try:
        scores.remove({})

        scores.insert({"name":"Jilly Bo","score":400,"time":str(datetime.now())})
        scores.insert({"name":"Bob","score":1539,"time":str(datetime.now())})
        scores.insert({"name":"AAA","score":744,"time":str(datetime.now())})

        print (list(scores.find().sort("score",DESCENDING)))
    except:
        print ('mongo unavailable')
        return False
    return True

try:
    # For clearing db
    if (len(sys.argv) > 1) and (sys.argv[1] == '-clear'):
        ClearScores()
except:
    print ('mongo unavailable')

