#!/usr/bin/env python

'''
face detection using haar cascades

USAGE:
    facedetect.py [--cascade <cascade_fn>] [--nested-cascade <cascade_fn>] [<video_source>]
'''

# Python 2/3 compatibility
from __future__ import print_function

import numpy as np
import pygame
import cv2 as cv

# local modules
from video import create_capture
from common import clock, draw_str


def detect(img, cascade):
    rects = cascade.detectMultiScale(img, scaleFactor=1.3, minNeighbors=4, minSize=(30, 30),
                                     flags=cv.CASCADE_SCALE_IMAGE)
    if len(rects) == 0:
        return []
    rects[:,2:] += rects[:,:2]
    return rects

def draw_rects(img, rects, color):
    for x1, y1, x2, y2 in rects:
        cv.rectangle(img, (x1, y1), (x2, y2), color, 2)

if __name__ == '__main__':
    import sys, getopt
    print(__doc__)

    args, video_src = getopt.getopt(sys.argv[1:], '', ['cascade=', 'nested-cascade='])
    try:
        video_src = video_src[0]
    except:
        video_src = 0
    args = dict(args)
    cascade_fn = args.get('--cascade', "../../data/haarcascades/haarcascade_frontalface_alt.xml")
    nested_fn  = args.get('--nested-cascade', "../../data/haarcascades/haarcascade_eye.xml")

    cascade = cv.CascadeClassifier(cascade_fn)
    nested = cv.CascadeClassifier(nested_fn)

    cam = create_capture(video_src, fallback='synth:bg=../data/lena.jpg:noise=0.05')

    while True:
        ret, img = cam.read()
        gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        gray = cv.equalizeHist(gray)

        t = clock()
        rects = detect(gray, cascade)
        vis = img.copy()
        draw_rects(vis, rects, (0, 255, 0))
        if not nested.empty():
            for x1, y1, x2, y2 in rects:
                roi = gray[y1:y2, x1:x2]
                vis_roi = vis[y1:y2, x1:x2]
                subrects = detect(roi.copy(), nested)
                draw_rects(vis_roi, subrects, (255, 0, 0))
        dt = clock() - t

        draw_str(vis, (20, 20), 'time: %.1f ms' % (dt*1000))
        cv.imshow('facedetect', vis)

        if cv.waitKey(5) == 27:
            break
    cv.destroyAllWindows()

class OpenCV_Test(object):
    def __init__(self, viz_num):
        super(OpenCV_Test, self).__init__()
        self.viz_num = viz_num
        self.cascade_fn = "./haarcascade_frontalface_alt2.xml"
        self.nested_fn  = "./haarcascade_eye.xml"
        self.cascade = cv.CascadeClassifier(self.cascade_fn)
        self.nested = cv.CascadeClassifier(self.nested_fn)
        self.desmurf = True
        self.face_count = 0
        self.cam = create_capture(0, fallback='synth:bg=./lena.jpg:noise=0.05')

    def start_viz(self, screen):
        ret, img = self.cam.read()
        gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        gray = cv.equalizeHist(gray)

        t = clock()
        rects = detect(gray, self.cascade)
        vis = img.copy()
        self.face_count = 0
        for x1, y1, x2, y2 in rects:
            self.face_count += 1
        draw_rects(vis, rects, (0, 255, 0))
        if not self.nested.empty():
            for x1, y1, x2, y2 in rects:
                roi = gray[y1:y2, x1:x2]
                vis_roi = vis[y1:y2, x1:x2]
                subrects = detect(roi.copy(), self.nested)
                draw_rects(vis_roi, subrects, (255, 0, 0))
        dt = clock() - t

        draw_str(vis, (20, 20), 'time: %.1f ms' % (dt*1000))
        #pygameSurface = pygame.surfarray.make_surface(vis)
        if self.desmurf:
            vis = cv.cvtColor(vis, cv.COLOR_BGR2RGB)
        pygameSurface = pygame.surfarray.make_surface(vis.swapaxes(0, 1))
        pygameSurface_scaled = pygame.transform.scale(pygameSurface, (screen.get_width(),screen.get_height()))
        screen.blit(pygameSurface_scaled, (0,0))
        return self.face_count

    def flip_desmurf(self):
        if self.desmurf:
            self.desmurf = False
        else:
            self.desmurf = True
    def finish_viz(self, screen):
        return 'nil'
