'''The MIT License (MIT)

Copyright (c) 2017 ActiveState Software Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.'''

import pygame
from pygame.locals import *
import sys
from itertools import cycle
from utils import *
from defs import *

pressed = ""
cur_char = 1
submit_name = False
alpha = ['END','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',' ']

def enter_text(eq, screen, max_length, lower = False, upper = False, title = False):
    global pressed
    global submit_name
    global cur_char
    global alpha
    allowed_values = [i for i in range(97, 123)] +\
                     [i for i in range(48,58)]

    BLINK_EVENT = pygame.USEREVENT + 0
    pygame.time.set_timer(BLINK_EVENT, 800)
    blinky = cycle(["_", " "])
    next_blink = next(blinky)
    displaytext('GAME OVER', 54, 320,60, FONT_COLOR, screen)

    if len(pressed) < max_length:
        displaytext('Enter your name:', 44, 320,125, FONT_COLOR, screen)
        displaytext('1 - enter the letter', 24, 32,225, FONT_COLOR, screen)
        displaytext('2 - next letter', 24, 32,275, FONT_COLOR, screen)
        displaytext('3 - previous letter', 24, 32,325, FONT_COLOR, screen)
        displaytext('4 - delete letter', 24, 32,375, FONT_COLOR, screen)
    else:
        cur_char = 0
        displaytext('1 - end and submit name', 24, 32,375, FONT_COLOR, screen)

    for event in eq:
        if event.type == BLINK_EVENT:
            next_blink = next(blinky)
        elif (event.type == pygame.KEYDOWN and (event.key == pygame.K_1)) or (event.type == pygame.JOYBUTTONDOWN and (event.button == 1)):
            # button 1 pressed enter letter
            if cur_char == 0:
                submit_name = True
            else:
                if len(pressed) < max_length:
                    pressed += alpha[cur_char]
        elif (event.type == pygame.KEYDOWN and (event.key == pygame.K_2)) or (event.type == pygame.JOYBUTTONDOWN and (event.button == 2)):
            # button 2 pressed next letter
            cur_char += 1
            if cur_char >= len(alpha):
                cur_char = 0
        elif (event.type == pygame.KEYDOWN and (event.key == pygame.K_3)) or (event.type == pygame.JOYBUTTONDOWN and (event.button == 3)):
            # button 3 pressed previous letter
            cur_char -= 1
            if cur_char <  0:
                cur_char = len(alpha) - 1
        elif (event.type == pygame.KEYDOWN and (event.key == pygame.K_4)) or (event.type == pygame.JOYBUTTONDOWN and (event.button == 4)):
            # button 4 delete letter
            pressed = pressed[:-1]
        #elif event.type == KEYUP and event.key in allowed_values \
       #     and len(pressed) < max_length:
            # caps entry?
            #if pygame.key.get_mods() & KMOD_SHIFT or pygame.key.get_mods()\
            #    & KMOD_CAPS:
            #    pressed += chr(event.key).upper()
            # lowercase entry
            #else:
            #    pressed += chr(event.key)
        # otherwise, only the following are valid inputs
        elif event.type == KEYUP:
            if event.key == K_BACKSPACE:
                pressed = pressed[:-1]
            elif event.key == K_SPACE:
                pressed += " "

    # only draw underscore if input is not at max character length
    if len(pressed) < max_length:
        displaytext(pressed + alpha[cur_char] + next_blink, 44, 320, 180, FONT_COLOR, screen)
    else:
        displaytext(pressed, 43, 320, 180, FONT_COLOR, screen)

    # perform any selected string operations
    if lower: pressed = pressed.lower()
    if upper: pressed = pressed.upper()
    if title: pressed = pressed.title()
