'''The MIT License (MIT)

Copyright (c) 2017 ActiveState Software Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.'''

from defs import *
import pygame
#import pygame.freetype
from random import randrange, choice

trainedBrain = None

def loadfont(size):
    global font
    #font = pygame.freetype.Font('font/De Valencia (beta).otf', size)
    #font = pygame.font.SysFont('DefaVu', 44)
    font = pygame.font.SysFont('DefaVu', size)


# Utility functions
#def displaytext(text, fontsize, x, y, color, surface):
    #global font
    #texcol = pygame.Color(*color)
    #text = font.render(text, texcol)
    #textpos = text[0].get_rect(centerx=x, centery=y)
    #surface.blit(text[0], textpos)

def displaytext(text, fontsize, x_pos, y_pos, color, screen):
    #global font
    font = pygame.font.SysFont('DefaVu', fontsize)
    font_surf = font.render(text, True, color)
    if x_pos == 'center':
        this_x_pos = int(screen.get_width()/2 - font_surf.get_width() /2)
    elif x_pos > 0:
        this_x_pos = x_pos
    if y_pos == 'center':
        this_y_pos = int(screen.get_height()/2 - font_surf.get_height() /2)
    elif y_pos > 0:
        this_y_pos = y_pos
    #rect = pygame.draw.rect(screen, BOX_COLOR, (this_x_pos - 10, this_y_pos -10, font_surf.get_width() + 20, font_surf.get_height() + 15), 0)
    #border_rect = pygame.draw.rect(screen, BORDER_COLOR, (this_x_pos - 10, this_y_pos -10, font_surf.get_width() + 20, font_surf.get_height() + 15), 2)
    screen.blit(font_surf, (this_x_pos, this_y_pos))

def init_stars(screen):
    """ Create the starfield """
    global stars
    stars = []
    for i in range(MAX_STARS):
    # A star is represented as a list with this format: [X,Y,speed]
        #star = [randrange(0,screen.get_width()/2 - 4),
        star = [randrange(0,screen.get_width() - 4),
                randrange(0,screen.get_height() - 1),
                choice([2,3,4])]
        stars.append(star)

def move_and_draw_stars(screen):
  """ Move and draw the stars in the given screen """
  global stars
  for star in stars:
    star[1] += star[2]
    # If the star hit the bottom border then we reposition
    # it in the top of the screen with a random X coordinate.
    if star[1] >= screen.get_height():
      star[1] = 0
      star[0] = randrange(0,1274)
      star[2] = choice([2,3,4])
 
    # Adjust the star color acording to the speed.
    # The slower the star, the darker should be its color.
    if star[2] == 2:
      color = (100,100,255)
    elif star[2] == 3:
      color = (190,190,190)
    elif star[2] == 4:
      color = (255,255,255)
 
    # Draw the star as a rectangle.
    # The star size is proportional to its speed.
    screen.fill(color,(star[0],star[1],star[2],star[2]))
